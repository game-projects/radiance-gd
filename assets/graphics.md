# Sonic Radiances - Graphics

L'objectif est d'avoir un style simple mais efficace, qui ne prend pas trop de temps à créer tout en gardant les éléments principaux de la saga. L'idée serait d'avoir une inspiration très proche de la simplicité d'un Mother, reprenant vraiment la simplicité et l'aspect "agréable à l'oeil" de Mother 3 ainsi que de Pokémon FRLG combiné a quelques éléments classiques reconnaissables des jeux SAdv et Battle (notamment les aspect esthétique globaux de la carte du monde de Sonic Battle)

## Inspirations :

- Mario & Luigi :: https://www.spriters-resource.com/game_boy_advance/mlss/ 

- Mother 3 :: https://www.spriters-resource.com/game_boy_advance/mother3/

- Pokemon FRLG : https://www.spriters-resource.com/game_boy_advance/pokemonfireredleafgreen/ (s'inspirer notamment pour tout ce qui est bâtiment modernes)

- Sonic Battle (notamment les cartes) : https://www.spriters-resource.com/game_boy_advance/sonicbattle

(+ les jeux précédants pour les aspects supplémentaires à reprendre)

## Note importante :

- NE PAS reprendre le style de GHZ si je ne veux pas me faire tuer par la commu. Les gens en ont trop marre. S'inspirer plutôt d'autres jeux.

## TODO :

- Tester ce style en refaisant Emerald Town de Sonic Battle entièrement en ce style (permettra aussi d'avoir une map de test).