# Sonic Radiance - music

## Theme principale

Le theme principale de Sonic Radiance sera une musique du groupe Crush40, afin de faire référence à l'ère Adventure de la saga. Les différentes possibilités discutées sont les suivantes :

- The light of the day :: Une musique positive, qui permettrait de lier le scénario à l'idée que les héros seraient des "gardiens".

- Two night to remember :: Concentration autour de deux nuits, musique pleine de vitalité.

- One of those days :: Une musique qui représenterait bien l'idée qu'on pourrait écrire les pires jours comme juste "un de ces jours". Pourrait parler à la fois du vécu des trois ados, et de l'histoire dans sa globalité.