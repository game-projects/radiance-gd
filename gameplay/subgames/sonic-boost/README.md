# Sonic Boost

## Principe de base

Sonic Boost est un sub-game avec un aspect un peu "mobile" inspiré de plusieurs sources, qui fera partie du projet Sonic Radiance. Ce subgame sera utilise dans le scénario pour des phases "rapides", dans des quêtes annexes comme mini-jeu/mini-niveau, et apparaitra en tant que bonus complet (un peu à la manière du jeu de kart de Sonic Adventure 2)

Ses inspirations sont les suivantes :

- Le premier fangame jamais créé ([Sonic Boom](https://www.youtube.com/watch?v=jYT28un2pEY)), ainsi que son remake ([Neo Sonic Boom](https://www.youtube.com/watch?v=MSofjiB_4J8&t=577s)). Ce jeu se veut sur certain point une combinaison de ce premier fangame avec d'autres sources d'éléments.

- Shadow Shoot, un jeu mobile SEGA du Sonic Café, qui va être la principale source des graphismes et du gameplay de base. Le jeu va reprenddre ses graphismes, et le fait de déplacer sur 5 rails (mais l'aspect "shoot" sera réservé à certains personnages).

- La série de vieux fangame de course "Sonic R GM", "Sonic R DX", "Sonic R MV" de NICKtendo DS

- Les jeux mobile Sonic récent (c'est à dire post-Sonic Café) comme Sonic Dash ou Sonic Runners

- Smash Bros, sur certains points (aspect "collection" et tout)

## Modes de jeux (si c'est possible à mettre dans les temps)

Sonic Boost sera un subgame avec quelques utilisations dans le mode histoire, et quelques modes bonus accessible pour plus de rejouabilité. Les passages en mode "Sonic Boost" seront rejouable dans le menu en plus d'être jouable dans le jeu principal.

### Utilisation dans le mode de jeu principal

Le jeu principal et ses quêtes annexes contiendront plusieurs moments où vous devrez foncer à travers des niveaux en utilisant le gameplay type Boost. Si dans l'aventure principal ils ne seront pas forcément très souvent utilisé (3~4 fois au total), ils seront plus régulièrement présent en tant que niveau bonus permettant d'obtenir des emblèmes supplémentaires, ou en faisant partie de quêtes secondaires.

TODO: Definir exactement quels types de manière dont on peut jouer à un niveau.

Chaque niveau déjà joué sera rejouable dans le menu principal. Ce mode de jeu jeu s'inspirera alors à la fois du mode "Brawl/Smash" de Smash Bros, et de la manière dont se joue les niveaux de Sonic R GM. En effet, vous pourrez choisir votre personnage, puis choisir vos niveaux et la manière dont vous voulez y jouer (mission, mode "rival", etc).

### Mode "Endless"

Ce mode de jeu s'inspire des jeux comme Sonic Runners et Sonic Dash. Vous courrez dans l'un des niveaux disponible, avec des chunks qui se rajouteront au fur et à mesure. Vous avez un temps limité, mais qui se rempli à chaque checkpoint. Ces niveaux devront d'abord être débloqué dans chaque environnement de l'île pour être rejouable dans le menu principal.

Vous n'avez cependant qu'une vie, et le niveau se termine dès que vous perdez.

### Mode Classic

Inspiré du mode classic de Smash Bros, et du premier fangame (Sonic Boom / Neo Sonic Boom).

Vous jouez une série de 7 niveaux, chacun se terminant par un boss. Les niveaux sont choisi de manière semi aléatoire (pour chaque place, il y aura 2 niveaux possible). Dans chaque niveau, vous pouvez trouver une clef, qui vous permettra d'aller dans le special stage ensuite (sauf si vous perdez une vie). Comme dans Smash Ultimate, chaque personnage jouable aura son propre mini "scénario".

Si vous réussissez tout les special stage, vous pouvez aller au dernier niveau spécial "Milky Way".

Certains niveaux auront des gimmicks particulier, basé sur les différentes manières de jouer du mode normal (qui servira aussi à choisir des missions)

### Mode Tournoi

Il sera possible de débloquer quatre "coupe" dans le jeu principal en allant parler à Jet. Chaque coupe terminée pourra être rejouée dans le menu principal.

Cela consistera en un nombre de 4 coupes de 16 niveaux qui seront forcément en mode rival. Il sera possible d'utiliser des Wisps pour déconcentrer les adversaires, avec des effets similaires à ceux du mode combat.

### Mode "Shadow Shoot"

Ce mode de jeu sera aussi utilisé pour créer un clone de Shadow Shoot sur Mobile. Il sera possible de le débloqué après une quête annexe mineure centrée sur Shadow.

NOTE : Cela pourrait simplement être le mode Classic de Shadow ?
