# Liste d'objets potentiels pour Radiance

## Objets de combat

### Objets de soin

- Chili Dog
- Healty Dog
- Hyper Dog
- Egg Dog
- Med Emmitter
- Med Wave

- Tonic Drink
- Tonic Infusion
- Tonic Potion
- Egg Drink
- Tonic Emmitter
- Tonic Wave

- Revival Gem (Revive, HP=10%)
- Life Gem (Revive, HP=100%)

- Antidote
- Cure-All Spray

### Anneaux / Power-Up

- Power Ring (provoque effet Hyper 5 tours)
- Ninja Ring (provoque effet Camouflé 5 tours)
- Focus Ring (provoque effet Focus 5 tour)
- Lucky Ring (provoque effet Lucky 5 tour)

- Shield (Effet "Fortifié")
- Fire Sheild (Effet "Fortif. Elem." de Feu)
- Thunder Shield (Effet "Fortif. Elem." de Foudre)
- Hydro Shield (Effet "Fortif. Elem." d'Eau)
- Magnetic Shield (Effet "Fortifié" + RingMaker)
- Invincibility (Effet "Invincible" pendant un tour)
- Speed Up (effet speedup pendant deux tour)


### Wisps

- R. Wisp : ×5, 2×2, 125%, Fire Damage
- O. Wisp : ×1, 3×3, 150%, NonElem, Affaibli
- Y. Wisp : ×1, 1×1, 200%, Rend Vulnérable
- C. Wisp : x1, 5×1, 125%, NonElem, Distrait/Aveugle
- V. Wisp : x1, 4×4, 80%, Dark, Endors
