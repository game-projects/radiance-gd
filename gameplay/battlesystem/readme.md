# Sonic Radiance - Battle System

Le système de combat de Sonic Radiance est un système de combat basée à la fois sur du tactical RPG et sur du JRPG classique. Les personnages et les ennemis sont placé sur une grille de 13×7 case, sur laquelle ils peuvent se déplacer de entre deux et quatre cases par tour.

Les personnages et ennemis agissent plusieurs fois par tour, entre 1 et 3 fois (hors augmentations comme le status *Speed Up*), et agissent à la suite du plus au moins rapide (avec un malus dans les "actions suivantes par tour", ce qui fait que les trois actions de Sonic ne seront pas forcément à la suite, mais cela pourra arriver tout de même face à des ennemis bien plus lent). Les actions sont choisies juste avant le tours (à la Octopath Traveller).

## Compétences utilisables

- **Attaquer** : le personnage fait une série de trois attaques consécutives à 33% de l'attaque. Chaque attaques consécutives peut faire séparément un critique. Certaines capacités passives pourront augmenter le nombre d'attaques pour leur puissance.

- **Compétences** : Le personnage utilise l'une des compétences qui lui est propre. Voir dans les fiches de personnages les compétences utilisables.

- **Objet** : utilisation d'un objet (voir objets)

- **Défendre** : Le personnage se défend, lui permettant d'éviter ce tour-ci les effets d'un piège s'il se retrouve dessus, et d'encaisser une partie des dégats jusqu'à son tour suivant.

- **Fuir** : le personnage tente de fuir.

## Stats

Le système de statistique de Sonic Radiance est le suivant. Les premières states sont les classiques "HP" et "PP", qui représentent respectivement les points dédiés à la vie et aux attaques spéciales

- **HP** :: les points utilisé pour la vie

- **PP** :: les points utilisé pour les POW Moves

À cela se rajoutent 6 autres statistiques :

- **STRENGHT/FORCE** : Affecte les dégats bruts des attaques physiques + skill non-physique en partie

- **DEFENSE** : Affecte la défence face aux attaques physiques + skills non physiques en partie.

- **CHAOS** : Fusionne avec STR et DEF pour affecter l'attaque et resistance aux skills physiques.

- **SPEED/VITESSE** : Affecte l'esquive/precision (légèrement), les chances de fuir et l'ordre dans le tour.

- **TECHNIQUE** : Affecte les dégats des pièges, la réussite des attaques purement status ou dont le status est l'effet principal, la puissance des soins et la puissance des Wisps.

- **LUCK/CHANCE** : Affecte les coup critiques (confrontation des "LUCK"), les chances d'effets secondaire bonus (idem), et peut aider face à certains effets (confusions).

## Apprentissage des attaques.

Les attaques s'apprennent à partir du niveau 5, tout les 4.375 niveau. Une attaque sur trois est un skill passif, avec un total de 6 skills passif et 12 skills actifs à apprendre, jusqu'au niveau 75.

*Apprentissages* : 2 de base + [9, 13, 18, 22, 26, 31, 35, 40, 44, 48, 53, 57, 61, 66, 70, 75]

*Apprentissage des attaques* : [9, 13, 22, 26, 35, 40, 48, 53, 61, 66, 75]

*Apprentissage des passifs* : [18, 31, 44, 57, 70]
