## Sonic Radiance - Battle Status

### Positif

- **Camouflé** (Hidden) : Esquive ×1.25

- **Focus** (Focus) : Critical ×1.33

- **Fortifié** (Shielded) : Damage taken ×0.66

- **Fortifié Élem'** (Elem. Shield) : Damage taken ×0.66, Elem. Damage when hit.

- **Pouvoir Élem'** (Elem. Power) : Damage sent ×1.33, Elem. Damage when hitting.

- **Hyper** (Hyper) : Damage sent ×1.5, Critical ×1.25.

- **Chanceux** (Lucky) : Enn' critical ×0.66, Side-effect ×0.66.

- **Invincible** : Dégat subit ×0.

- **Acceléré** (Speed'Up) : 1 attaque supplémentaire/tour.

- **Attrap'Anneaux** (Ring) : Ring ×1.5 à la fin du combat

### Négatif

- **KO** (KO) : Ne peut plus agir, 0PV.

- **Empoisonné** (Poison) : -15% PV / tour

- **Affaibli** (Weakened) : Dommage envoyé ×0.66

- **Vulnérable** (Vulnerable) : Dommage subit ×1.33

- **Paralysé** (Paralised) : N'a que 50% de pouvoir agir/se déplacer.

- **Endormi** (Asleep) : Ne peut plus agir ni se déplacer. 30% de chance de se reveiller chaque tour, dure 5 tour max.

- **Maudit** (Cursed) : Crtiques ennemis ×1.33, effet secondaire ×1.33.

- **Distrait** (Distracted) : Précision des attaques ×0.66
