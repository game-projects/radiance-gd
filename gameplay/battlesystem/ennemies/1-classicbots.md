# Sonic Radiance - E-00 : Classic Badniks

**Note :** *Ces badnics ne suivent pas exactement la numérotation de Sonic Adventure, même si j'essaie de la reprendre autant que possible.*

- **E-01 Baby Kiki** - Prendre un Kiki de SAdv1 http://sonic.wikia.com/wiki/Kiki

- **E-02 Rhinotank** - Prendre un rhinotank de SAdv1 http://sonic.wikia.com/wiki/Rhinotank

- **E-03 Motobug** - Akatento de SAdv3 http://sonic.wikia.com/wiki/Akatento

- **E-04 Leon** - Leon de SAdv3 - http://sonic.wikia.com/wiki/Leon_(Badnik)

- **E-05 Catterkiller** - Guruguru http://sonic.wikia.com/wiki/Guruguru

- **E-06 Spinner** - Spinner de Colors DS http://sonic.wikia.com/wiki/Bladed_Spinner

- **E-07 IceBot** - Yukigasen de SAdv3 - http://sonic.wikia.com/wiki/Yukigasen - ou Yukimaru - http://sonic.wikia.com/wiki/Yukimaru

- **E-08 Circus Kiki** - Circus Kiki from SAdv2 http://sonic.wikia.com/wiki/Circus

- **E-09 BuzzBomber** - Buzzer recolo de SAdv/SAdv2

- **E-10 Crabmeat** - GamiGami from SAdv - http://sonic.wikia.com/wiki/GamiGami

- **E-11 Bubbles** - Puffer/Senbon from SAdv - http://sonic.wikia.com/wiki/Senbon

- **E-12 BatBot** - Batbot de Colors DS - http://sonic.wikia.com/wiki/Batbot

- **E-13 Turbo Spiker** - Yadokka de SAdv3 - http://sonic.wikia.com/wiki/Yadokka

- **E-14 Flying Motobug** - Aotento de SAdv3 - http://sonic.wikia.com/wiki/Aotento

- **E-15 Orbinaut** - Orbinaut from Sonic Colors DS - http://sonic.wikia.com/wiki/Orbinaut

- **E-16 Electro Spinner** - Electro Spinner from Sonic Colors DS - http://sonic.wikia.com/wiki/Electro_Spinner

- **E-17 Ghora** - Ghora from Sonic Advance 2 - http://sonic.wikia.com/wiki/Gohla

- **E-18 Catterkiller Jr.** - Wamu de Sonic Advance http://sonic.wikia.com/wiki/Wamu

- **E-19 Crabmeat MkII** - https://www.spriters-resource.com/ds_dsi/sonicrushadventure/sheet/20553/

- **E-20 Kiki** - Mon from Sonic Advance 2 - http://sonic.wikia.com/wiki/Mon

- **E-21 Buzzer** :: Buzzer de SAdv3 ou Colors DS

- **E-22 Slicer** - Kamaki de SAdv3 - https://sonic.fandom.com/wiki/Kamaki

- **E-23 Fire Catterkiller** :: Geji-Geji from Sonic Advance 2 - http://sonic.wikia.com/wiki/Geji-Geji

- **E-24 Jetso** - Condor de SAdv3 - http://sonic.wikia.com/wiki/Condor

- **E-25 Flickey** - Flickey de SAdv2 - http://sonic.wikia.com/wiki/Flickey

- **E-26 Bell** - Bell de SAdv2

- **E-27 Grabber** - Kyaccha de SAdv3 - http://sonic.wikia.com/wiki/Kyacchaa

- **E-28 Chopper** - Chopper de Sonic Colors DS

- **E-29 Egg Pirate** - Pirate avec tonneau de SRA https://www.spriters-resource.com/ds_dsi/sonicrushadventure/sheet/20558/

- **E-30 Slot** :: Slot from Sonic Advance

- **E-31 Grounder** :: Mogu from Sonic Advance http://sonic.wikia.com/wiki/Mogu

- **E-32 Unidus** :: Orbinaut recoloré de Colors DS suivant le design Adventure

- **E-33 Jawz** :: Jawz de Colors DS

- **E-34 Plated Spinner** :: Plated Spinner de Colors DS

- **E-35 Mole** :: Mole de Colors DS

- **E-36 Penguinator** :: Pen de Sonic Advance 2 - http://sonic.wikia.com/wiki/Pen

- **E-37 Iceball** - https://www.spriters-resource.com/ds_dsi/sonicrushadventure/sheet/20555/

- **E-38 Bubu** - Bu-bu de SAdv3 http://sonic.wikia.com/wiki/Bu-bu

- **E-39 GaoGao** - (robot lion jouet) - http://sonic.wikia.com/wiki/Gaogao

- **E-99** : Eggrobo
