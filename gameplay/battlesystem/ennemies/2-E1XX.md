## Série E-100

- **E-100 Zero** (Alpha) - BOSS: le robot qui a poursuivit Amy.

- **E-101 Chaos Beta** - BOSS: Une version reconstruite et améliorée de E-101

- **E-102 Chaos Gamma** - BOSS: Une version reconstruite et améliorée de E-102

- **E-103 Chaos Delta** - BOSS: Une version reconstruite et améliorée de E-103

- **E-104 Chaos Epsilon** - BOSS: Une version reconstruite et améliorée de E-104

- **E-105 Chaos Zeta** - BOSS: Une version reconstruite et améliorée de E-105

- **E-1XX** :: Variante produite en masse de la série E-100, gris. (fusion entre les E-1000 de SA2 et les Guard Robots de Battle)
