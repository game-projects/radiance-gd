# Sonic Radiance - Ennemies

Il va y avoir plusieurs types d'ennemis dans Sonic Radiance : les robots d'Eggman sont les principaux ennemis, mais vous pourrez également affronter des robots du G.U.N. voir d'autres personnages.

## Types d'ennemis dans le dossier :

- *classicbots* : les ennemis "classiques" (basé sur les badnics d'Advance)

- *E-1XX* : Des BOSS/mini-boss basé sur des versions améliorées de la série E-100 apparue dans Sonic Adventure.

- *neophi* : Des nouvelles versions améliorées des Phi, en série E-200. Au lieu de se baser sur un seul personnage, ils en fusionnent plusieurs. Serviront de boss au tout début, puis de mini-boss, deviendront plus fréquent en fin de jeu en tant qu'ennemi normaux.

- *pawn* : Les Egg Pawn, ennemis "normaux" plus puissant que les précédants.

- *gun* : Les ennemis venant du GUN, qui seront utilisé surtout dans certains endroits.
