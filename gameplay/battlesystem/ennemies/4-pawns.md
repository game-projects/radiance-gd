# Sonic Radiance - Série E-1000+ (Egg Pawn)

- **E-1001 Egg Pawn** - https://www.spriters-resource.com/resources/sheets/10/10012.png

- **E-1002 Egg Flapper** - https://www.spriters-resource.com/resources/sheets/10/10012.png

- **E-1003 Egg Pterodactyl** - https://www.spriters-resource.com/ds_dsi/sonicrushadventure/sheet/20551/

- **E-1004 Kuragen** - https://www.spriters-resource.com/resources/sheets/10/10012.png

- **E-1005 Egg Gosth** - https://www.spriters-resource.com/ds_dsi/sonicrushadventure/sheet/20554/

- **E-1006 Egg Hammer** - https://www.spriters-resource.com/ds_dsi/sonicrush/sheet/20550/

- **E-1007 Shield Pawn** - https://www.spriters-resource.com/fullview/10012/

- **E-1008 Falco** - https://www.spriters-resource.com/resources/sheets/10/10012.png

- **E-1009 Bat Flapper** - https://www.spriters-resource.com/fullview/10012/

- **E-1010 Egg Magician** - https://www.spriters-resource.com/resources/sheets/10/10012.png

- **E-1011 Knight Pawn** - https://www.spriters-resource.com/fullview/10012/

- **E-1012 Rhinoliner** - https://www.spriters-resource.com/ds_dsi/sonicrushadventure/sheet/20551/

- **E-1013 Laser Flapper** - https://www.spriters-resource.com/fullview/10012/

- **E-1014 Electro Kuragen** - https://www.spriters-resource.com/fullview/10012/

- **E-2006 Egg Gunner**
