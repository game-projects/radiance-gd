# Sonic Radiance - E-200 Néo-phi

Des robots basé sur les technologies de Gemerl et des Phi, combinant les capacités de plusieurs personnages en un seul.

- **E-200 Speed Phi** : Un Néo-phi mélangeant quelques capacités de Sonic, Espio et Shadow (bleu).

- **E-201 Brawler Phi** : Un Néo-phi mélangeant quelques capacités de Knuckles, Rouge et Amy (rouge).

- **E-202 Stealth Phi** : Un Néo-phi mélangeant quelques capacités de Rouge et Espio (violet).

- **E-203 Healer Phi** : Un Néo-phi mélangeant quelques capacités de Tails et Cream (blanc).

- **E-204 Trickster Phi** : Un Néo-phi mélangeant quelques capacités de Tails et Rouge.

- **E-205 Power Phi** : Un Néo-phi mélangeant quelques capacités de Shadow et Knuckles.
