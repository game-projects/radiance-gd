# Sonic Radiance - Game Design et concepts

Sonic Radiance est un fan-RPG au tour-par-tour Sonic avec un moteur de combat inspiré RPG tactique, et des inspirations des Mario et Luigi. L'objectif du jeu est de mélange l'ambiance des Sonic Adventure, Sonic Advance et Sonic Battle dans un enrobage RPG. Parmi les autres sources d'inspiration, on peut trouver une grande partie des RPG de l'époque GBA/DS, avec l'idée d'avoir un lieu riche à découvrir, avec de nombreux secrets et éléments supplémentaires pour le joueur.

Le but est également de garder certains éléments “centraux” des jeux Sonic, tels que les ranks, la vitesse, l’aspect action. Cela sera fait grace à un système de combat spécial offrant au joueur des capacités d’agir plusieurs fois et évitant les trop long “temps d’attente” où le joueur ne fait que regarder le combat se dérouler. L’overworld quant à lui sera en 2D vue de haut, avec des capacités sur le terrain gardant les origines “plateforme” de la série. De plus, des mini-jeux seront présent, visant à rendre le jeu plus dynamique.

## Synopsis

*Le scénario n'a pas encore été totalement décidé, voir story-concepts pour les concepts de lieu, lore et histoire.

## Licence

Le code du jeu est sous licence MIT. Les assets originaux sont © SEGA, et les assets et histoire sont propriété de leurs créateurs respectifs.
