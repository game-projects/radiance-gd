# Sonic the Hedgehog

Le hérisson le plus rapide du monde est de retour ! Orgueilleux, fier et inarrêtable, Sonic est toujours l'amoureux de la liberté et de la course qu'il a toujours été. Étonnamment plus malin qu'il le laisse paraître, le hérisson a été appellé par le dirigeant de la zone avec Tails et Amy afin d'aider face à une invasion de Badniks.

Toujours heureux de pouvoir aider les gens, il n'a pas hésité et le voilà à présent pour convattre Egman.

## Caractéristiques principales

- Type Speed

- Agit 3× par tour

- Est pas extrèmement fort mais rapide et avec une bonne précision

- Peut passer en super state.

### Sur le terrain

> Note : ces capacités de terrain seront pour certaine possible à acquérir et utilisable après l'obtention de certains powerup, à la Sonic Adventure 1 ou 2

- A : Saut

- A+A : Jump Dash (+ Homming Dash si powerup)

- B : Course (permet d'aller plus vite, et de passer certains obstacle)

- B au bout d'un moment : Dash mode, permet de courir sur l'eau notamment (si powerup)

- CONTEXTUEL : Light Dash

- PASSIF : *À trouver*

## Système de combat

- **HP** :: Rank B

- **PP** :: Rank C

- **Attack** :: Rank B

- **Defense** :: Rank B

- **Technique** :: Rank C

- **Power** :: Rank C

- **Mind** :: Rank D

- **Speed** :: Rank S

### Techniques

- Spin Attack :: une course tourbillonante attaquant un ennemi au sol sur trois case devant le personnage.

- Spin Dash :: une course tourbillonante chargée attaquant un ennemi au sol sur cinq case devant le personnage. Le personnage se défend automatiquement pendant le tour de charge et inflige des dégats sur les techniques direct.

- Homming Attack :: Le personnage peut viser un ennemi autour de lui sur un rayon de deux case, le frappant automatiquement. Touche les ennemis aérien. Avec les niveaux, ils peut viser plus d'ennemi.

- Light Speed Attack :: Technique ultime Speed - homming attack sur tous les ennemis du terrain.

- Boost :: Le personnage attaque un ennemi sur cinq case devant lui, traverse les ennemis.

- Blue Tornado :: Le personnage balance une tornade attaquant sur un carée de 3×3 case devant lui. Les ennemis au centre se prennent plus de dégats. Supprime les bouclier.

- Sonic Wind :: Brise les bouclier de tous les ennemis devant le personnage.

- Sonic Cracker (piège) : Le personnage pose un piège qui explose au contact d'un ennemi.

- Sonic Flare : Le personnage attaque tous les ennemis autour de lui, attaque lourde.

- Sonic Wave : Le personnage envoie une vague d'énergie devant lui. Peut étourdir les ennemis.

- Speed Up : Le personnage produit l'effet "Speed Up" à un personnage au choix, lui apportant une attaque supplémentaire.

- Time Stop : Le personnage produit l'effet "figé" à un ennemi pendant 1 tours.
