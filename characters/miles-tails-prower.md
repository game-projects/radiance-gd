# Miles "Tails" Prower

Le renardeau suivant Sonic depuis (presque) toujours. Inventif et intelligent, Tails n'est pas autant un combattant que ses amis mais à appris à se débrouiller en combat. Il souffre d'un manque de confiance en lui, qu'il a commencé à apprendre à dépasser, mais Rome ne s'est pas faite en un jour.

## Caractéristiques principales

- Type Technique

- Agit 2× par tour

- Est le deuxième plus faible des perso principaux, mais peut faire pas mal d'effets secondaire et est bon avec les objets. Personnage équilibré de soutiens.

- Peut passer en super state.

### Sur le terrain

> Note : ces capacités de terrain seront pour certaine possible à acquérir et utilisable après l'obtention de certains powerup, à la Sonic Adventure 1 ou 2

- A : Saut

- A+A : Vole

- B : Arm Canon (permet de toucher des éléments à distance)

- CONTEXTUEL : Interragit avec les machine.

- PASSIF : *À trouver*

## Système de combat

- **HP** :: Rank C

- **PP** :: Rank A

- **Attack** :: Rank D

- **Defense** :: Rank C

- **Technique** :: Rank S

- **Power** :: Rank C

- **Mind** :: Rank A

- **Speed** :: Rank B

### Techniques

- Spin Attack :: une course tourbillonante attaquant un ennemi au sol sur trois case devant le personnage.

- Spin Dash :: une course tourbillonante chargée attaquant un ennemi au sol sur cinq case devant le personnage. Le personnage se défend automatiquement pendant le tour de charge et inflige des dégats sur les techniques direct.

- Adrénaline Rush :: Le personnage peut activer un mode Hyper sur un personnage au choix.

- Chu² Bomb :: Un piège qui traverse le terrain jusqu'à un ennemi (1 case/tour)

- Dummy Ring Bomb :: Lance trois anneau paralysant devant le personnage.

- Energy Shot :: Attaque à distance paralysante sur un ennemi en avant (5 case)

- Mecha Hook :: Lance un coup de poing mécanique tout autour de lui. Peut étourdir

- Magic Upper :: Lance un coup de poing concentré puissant sur une case devant lui.

- Tail Attack :: Un coup de queue qui attaque sur trois case devant le héros.

- Rapid Tail Attack :: Un coup de queue tournoyant attanquant tout autour plusieurs fois.

- Scan :: Permet d'obtenir les informations sur un ennemi

- Energy Laser :: Tir perforant touchant tous les ennemis en avant.

- Ajouter un Heal
