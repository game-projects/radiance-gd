# Espio the Hedgehog

Ninja des Chaotix, Espio est sérieux et concentré, et se laisse peu distraire par le divertissement et autres choses du genre.

Il est présent pour enquêter sur la société Rimlight, et sur ses rapports avec GUN. Il s'intéressera particulièrement à l'affaire de la Gosth Central.

## Caractéristiques principales

- Type Speed

- Agit 2× par tour

- A pas mal de capacité pour esquiver. Basé pas mal sur les critiques.

- Ne peut pas passer en super state.

### Sur le terrain

> Note : ces capacités de terrain seront pour certaine possible à acquérir et utilisable après l'obtention de certains powerup, à la Sonic Adventure 1 ou 2

- A : Saut

- A+A : Jump Dash (+ Homming Dash si powerup)

- B : Kunai, touche à distance

- CONTEXTUEL : *À trouver*

- PASSIF : *À trouver*

## Système de combat

- **HP** :: Rank B

- **PP** :: Rank C

- **Attack** :: Rank A

- **Defense** :: Rank C

- **Technique** :: Rank B

- **Power** :: Rank C

- **Mind** :: Rank A

- **Speed** :: Rank A

### Techniques


- Shuriken

- Expl. Kunai

- Chroma Camo (augmente esquive pd 3 tours, passe Espio dans son set de Sprite "invisible")

- Leaf Swirl (tornade qui touche tout les ennemis autour et rend espio invisible 1 tour)

- Chaotic Tornado (Espio tournoi et provoque des dégats tout autour)

- Ninja Trap (Espio fait apparaitre un explosif dans le sol)

- Tornado Dash (Espio tournoi et attaque en avant sur qq cases)

- Poison Kunai

- Ninja Trick (produit un effet faibless aux ennemis autour)

- Ninja Focus (le personnage reçoit le status "FOCUS")
