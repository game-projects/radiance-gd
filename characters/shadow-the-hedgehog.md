# Shadow the Hedgehog

L'espèce vivante suprême, un hérisson sombre et peu loquace. Rarement la personne la plus sympathique avec qui parler, parfois trop sûr de sa propre puissance, et plus souvent concentré sur sa mission que sur aider chaque personnage sur son passage, Shadow n'en est pas moins quelqu'un de dévoué pour protéger la planète.

Il est ici afin de découvrir qui est le mystérieux "traitre" qu'il y aurait dans l'administration de la zone.

## Caractéristiques principales

- Type Speed

- Agit 3× par tour

- Personnage le plus puissant à distance, et bon en vitesse. Peu de PV et

- Peut passer en super state

### Sur le terrain

> Note : ces capacités de terrain seront pour certaine possible à acquérir et utilisable après l'obtention de certains powerup, à la Sonic Adventure 1 ou 2

- A : Saut

- A+A : Jump Dash (+ Homming Dash si powerup)

- B : Course (permet d'aller plus vite, et de passer certains obstacle)

- B au bout d'un moment : Dash mode, permet de courir sur l'eau notamment (si powerup)

- CONTEXTUEL : Light Dash

- PASSIF : *À trouver*

## Système de combat

- **HP** :: Rank D

- **PP** :: Rank B

- **Attack** :: Rank A

- **Defense** :: Rank D

- **Technique** :: Rank C

- **Power** :: Rank S

- **Mind** :: Rank D

- **Speed** :: Rank A

### Techniques

- Spin Attack

- Spin Dash

- Chaos Boost (boost temporairement ses stats)

- Chaos Control (comme le Time Stop en plus puissant)

- Chaos Blast (dégats à tout les ennemis)

- Chaos Burst (attaque autour)

- Chaos Rift (fusion avec Chaos Magic de Sonic Battle)

- Chaos Nightmare/Sphere (affect les deux côtés, ralenti les ennemis)

- Chaos Spear (attaque droit devent, perce les armures)

- Maria's Wish (regénération)

- Chaos Lance

- Light Speed Attack
