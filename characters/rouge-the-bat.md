# Rouge the Bat

Espionne du gouvernement, intelligente, voleuse et aimant bien se jouer des autres. Aimant particulièrement taquiner Knuckles et tenter de lui voler la master emerald - bien qu'on peut se demander si le fait que ça énerve l'échidné n'est pas en soi une raison suffisante.

Elle est présente dans la base pour faire des recherche sur le projet Gosth Central et la manière dont Eggman à obtenu des informations dessus. Elle rejoindra l'équipe avec des informations sur le projet Gosth Central, et sur le rôle de Rimlight dans toute cette histoire.

## Caractéristiques principales

- Type technique

- Agit 2× par tour

- Personnage technique assez puissant en combat, avec pas mal de capacité pour mettre des altération d'état

- Ne peut pas passer en super state

### Sur le terrain

> Note : ces capacités de terrain seront pour certaine possible à acquérir et utilisable après l'obtention de certains powerup, à la Sonic Adventure 1 ou 2

- A : Saut (avec chute ralenti à la Battle ?)

- A+A : Vole

- B : Coup de pied (détruit objet fragile + plus solide si powerup)

- CONTEXTUEL : peut creuser.

- PASSIF : Détecteur de trésor.

## Système de combat

- **HP** :: Rank C

- **PP** :: Rank A

- **Attack** :: Rank D

- **Defense** :: Rank C

- **Technique** :: Rank A

- **Power** :: Rank B

- **Mind** :: Rank C

- **Speed** :: Rank D

### Techniques

- Spin Drill

- Upper Attack (Secret Kick)

- Black Wave

- Bat Kick

- Bat Bomb

- Beauty Choc

- Charly Kick (coup de pied qui attaque tout autour)

- Distract (Generation / Chronicles)

- Jewel Storm ?

- Shriek

- Plunder

- Silent Size

- Attaque à la "vampirisme" de pokémon.
