# Amy Rose

Plus grande fan autoproclamée de Sonic devenue une ami fidèle du hérisson, Amy Rose est une force de la nature en plus d'être une personne pleine de compassion et d'énergie. Cette jeune hérissonne ne désespère pas de réussir à séduire Sonic, mais est également capable de penser au plus important.

Elle est présente avec Tails et Sonic pour élucidé les mystères de la zone.

## Caractéristiques principales

- Type power

- Agit 2× par tour

- Moins forte que Knuckles, mais plus de technique et quelques capacités de soin

- Peut passer en super state? (j'ai les sprites pour, donc pourquoi pas ?)

### Sur le terrain

> Note : ces capacités de terrain seront pour certaine possible à acquérir et utilisable après l'obtention de certains powerup, à la Sonic Adventure 1 ou 2

- A : Saut

- A+A : Double Saut

- B : Coup de Marteau (permet de détruire des élément du décors)

- CONTEXTUEL : *A trouver*

- PASSIF : *A trouver*

## Système de combat

- **HP** :: Rank A

- **PP** :: Rank C

- **Attack** :: Rank A

- **Defense** :: Rank B

- **Technique** :: Rank C

- **Power** :: Rank B

- **Mind** :: Rank C

- **Speed** :: Rank B

### Techniques

- Rose Heal (soigne un personnage) //A REMPLACER

- Tarot Curse (maudit un ennemi)

- Amy Flash (affecte tout les ennemis avec un effet)

- Piko Tornado (G.Tornado de S. Battle)

- Gift Trap

- Hammer Attack

- Pink Tornado

- Rose Typhon

- Spin Hammer Attack

- Storming Heart (comme le Amy Flash, mais sur un ennemi + quelques dégats)
