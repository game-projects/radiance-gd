# Knuckles the Echidna

Knuckles est le gardien de la Master Emerald, ayant vécu en ermite depuis toujours sur Angel Island, l'ile flottante où la gemme mystique repose. Cet echidné prend son rôle au sérieux, et ne supporte pas que quiconque pose la main dessus. Dupé mainte fois par le passé, il est désormais plus prudent.

*TODO:* Déterminer la raison de sa présence.

## Caractéristiques principales

- Type power

- Agit 2× par tour

- Personnage le plus fort, assez tanky, mais peu resistant sur le mental

- Peut passer en super state

### Sur le terrain

> Note : ces capacités de terrain seront pour certaine possible à acquérir et utilisable après l'obtention de certains powerup, à la Sonic Adventure 1 ou 2

- A : Saut

- A+A : Plane

- B : Coup de Poing (permet de détruire des élément du décors)

- CONTEXTUEL : Creuse

- PASSIF : Détecteur de trésor.

## Système de combat

- **HP** :: Rank A

- **PP** :: Rank D

- **Attack** :: Rank S

- **Defense** :: Rank A

- **Technique** :: Rank C

- **Power** :: Rank C

- **Mind** :: Rank D

- **Speed** :: Rank C

### Techniques

- Spin Attack

- Spin Dash

- Deep Impact

- Spin Drill

- Enrage

- Ground Shaker / Hammer Punch (touche tout les ennemis sur une grande zone, utilise le Knux. Chop comme sprite)

- Uppercut

- Maximum Heat Knuckles

- Mole Bomb

- Meteor Crush

- Thunder Arrown

- Volcanic Dunk

- Guardian Heal
