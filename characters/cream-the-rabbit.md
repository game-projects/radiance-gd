# Cream the Rabbit

Cream est la fille de Vanilla the Rabbit, une petite lapine calme et polie qui n'aime pas se battre. Cependant courageuse, elle n'hésite pas à venir à ses ennemis en aide. Ce n'est pas le danger qui lui fait peur, mais le fait de ne pas aimer la violence.

Elle rejoindra l'équipe après avoir été lors d'une quête annexe.

*TODO:* Déterminer la raison de sa présence.

## Caractéristiques principales

- Type Technique

- Agit 2× par tour

- La plus faible en force et en pouvoir, Cream est cependant la meilleurs healeuse du jeu.

- Ne peut pas passer en super state.

### Sur le terrain

> Note : ces capacités de terrain seront pour certaine possible à acquérir et utilisable après l'obtention de certains powerup, à la Sonic Adventure 1 ou 2

- A : Saut

- A+A : Vole

- B : Chao Dash (permet de toucher des éléments à distance)

- CONTEXTUEL : *À trouver*

- PASSIF : *À trouver*

## Système de combat

- **HP** :: Rank A

- **PP** :: Rank B

- **Attack** :: Rank D

- **Defense** :: Rank B

- **Technique** :: Rank A

- **Power** :: Rank C

- **Mind** :: Rank A

- **Speed** :: Rank C

### Techniques

- Chao Attack

- Heal Cheer

- Revive Cheer

- Thoughness Cheer

- Refresh Cheer

- Cure Cheer

- Demoralize

- Omochao Trap

- Chao Tornado (Chao Rush de S. Battle)

- Chao Canon
