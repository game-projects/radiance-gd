# Sonic Radiance - Monado Prime

Monado Prime est dans cette version une reprise de l'ancien concept d'Utopic Circuits : une base en circuit fermé inspiré des projets de biospheres comme [Biosphère 2](https://fr.wikipedia.org/wiki/Biosph%C3%A8re_II), [BIOS-3](https://en.wikipedia.org/wiki/BIOS-3) ou [MELiSSA](https://en.wikipedia.org/wiki/MELiSSA). Le but est également de s'inspirer un peu du BSL de Metroid Fusion et du Bottle Ship de Other M.

Monado Prime est donc un système écologique en circuit fermé, composé d'un espace de vie principal, d'usine bactérienne biologique (s'occupant du plus gros de recyclage de l'air et des déchets biologiques), de biomes artificiels, d'un espace numérique autonome, ainsi que de tout un espace de laboratoires et de stations technologique. Il s'agit de la première station (d'où le nom Prime) de type Monado. Le terme monado indique le principe de base d'un écosystème clos : être un monde hermétique, une sorte d'univers fermé, tel les monades de Leibniz.

La base est composé d'une zone principale (Hermetic Utopia, notée MP0), de sept biômes artificiels (l'Agrosphère, la Phytosphère, l'Hydrosphère, la Thermosphère, la Cryosphère, la Limnisphère et la Lithosphère, notés de MP1 à MP7) et de trois secteur interdits (les Wrath Corridor, la Gosth Central et l'Hybris Altar, noté MPX à MPZ).

Elle est située sur l'ile de Fortitude Rocks, qui est une île rocheuse polaire. Les rares moments en extérieur montreront donc des biomes types glace/montagnes.

Ce monde peut donc contenir des zones assez différentes, suivant les besoins.

*Note* : Ce projet se rapprochant plus des Sonic Adventure contrairement aux instances précédantes, l'idée serait d'avoir 11 zones principales (à la Adventure).

## Liste des zones

( Techniquement, en plus de ces zones existe l'extérieur nommé "Fortitude Rock" )

### MP0 - Hermetic Utopia, la plus grande ville

*La ville principale de la base et plus grand secteur de toute la biosphère, Hermetic Utopia sert de point névralgique entre les différentes sphères. Elles contient un grand Adventure Field, quelques corridors, et pas mal d'Action Stage unique où vous reviendrez au fur et à mesure du jeu.*

- **Adventure Fields** :

- **Action Stages** :

### Les sept biomes

#### MP1 - L'Agrosphère

*La sphère des grandes plaines, plus ou moins humides, et de l'agriculture. Cette sphère a pour but de recréer un espace tempéré commun et simple. Plus qu'une sphère d'expérimentation, elle a pour but de permettre de nourrir toute la base, même si d'autres types d'aliment sont produit dans les autres sphères. C'est la sphère la plus habitée*

- **Adventure Fields** :

- **Action Stages** :

#### MP2 - La Phytosphère

*La sphère des grandes forêts et des espaces boisés sauvages. Cette sphère à pour but de recréer tout les espèces un peu sombre et les grandes forêts immenses. Elle a été séparée de l'Agrosphère, dans le but que ce que les besoins de production d'aliment n'empiète pas sur les expérimentation sociale. Un petit village forestier, Woody Village, s'y trouve.*

- **Adventure Fields** :

- **Action Stages** :

#### MP3 - L'Hydrosphère

*La sphère de l'océan, des mers et également des îles cotières. Si l'objectif de cette sphère est avant-tout de recréer un écosystème marin, elle est rapidement devenu également la sphère parfaite pour passer ses vacances. Qui croirait qu'il existe tout un paradis côtier au milieu de l'ile de Fortitude Rock ?*

- **Adventure Fields** :

- **Action Stages** :

#### MP4 - La Thermosphère

*La sphère des déserts, de l'aridité et des environnements plus difficile. Beaucoup ne voyant qu'un aspect pratique à la biosphère se demande pourquoi une telle sphère existe. Bien plus scientifique du coup que les autres sphères, elle est également la sphère des laboratoires*

- **Adventure Fields** :

- **Action Stages** : Sillicon Discord (circuit informatiques, controlés en partie par Eggman).

#### MP5 - La Cryosphère

*La sphère des grands froids et de la neige. En effet, même si techniquement l'extérieur de la base est également glacial, Fortitude Rock est également aride. De plus, le but de la biosphère étant d'être un écosystème entier hermétique, il n'est pas possible d'avoir quelque chose à l'extérieur. C'est ici que se trouve la nouvelle centrale*

- **Adventure Fields** :

- **Action Stages** : Power Roots (centrale énergétique)

#### MP6 - La Limnisphère (marécages / jungle / mangrove)

*La sphère des espaces humides (jungles et marécages chaud). C'est ici aussi qu'on y trouve les pousses de plantes humides. Cette sphère a été victime d'une contamination lors de l'incident de la première centrale de la base, c'est donc un espèce en grande partie interdit au publique. C'est ici qu'on également été déplacé toutes les machineries pour lutter contre la contamination stygienne*

- **Adventure Fields** :

- **Action Stages** : Corrupted Sphere (espace corrompu)

#### MP7 - La Lithosphère (roche/montagne)

*La sphère des espaces rocheux et montagneux, des steppes et des cavernes sombre. Avec son vent artificiel, l'espace rappelle beaucoup les montagnes et les grandes espaces tempérés mais secs. C'est également un espace servant à générer l'oxygène, contenant donc de nombreuses usines et espaces sécurisé, et étudiant les espèces montagneuse ou souteraine*

- **Adventure Fields** :

- **Action Stages** :

### Les espaces fermés au public

#### MPX - Wrath Corridor

*Les espèces réservés à GUN de la base. Ils forment l'étage -1 de la base, et sont interdit au public pour des raisons de sécurités, formant notamment un tampon entre les espaces habités et l'ancienne centrale chaotique*

- **Adventure Fields** :

- **Action Stages** :

#### MPY - Gosth Central

*L'ancienne centrale chaotique de la base, aujourd'hui désaffecté… Enfin, ça c'était avant qu'Eggman en fasse sa base principale de contrôle, profitant des portes dérobée installé par le général de GUN de l'époque pour s'assurer que personne d'autre que lui en prenne le contrôle.*

- **Adventure Fields** :

- **Action Stages** :

#### MPZ - Hybris Altar

*L'espace au cœur du centre de la base. L'ancien temple construit autour du Stygian Rift. Il est traversé par les tuyaux connectant la Gosth Central à la veine chaotique corrompue. La notion d'hybris évoque tout ceux qui ont pensé pouvoir contrôler cette source d'énergie incontrôlable.*

- **Adventure Fields** :

- **Action Stages** :

### Autres

BOSS FINAUX : **APATHY** et **CATHARSIS**
