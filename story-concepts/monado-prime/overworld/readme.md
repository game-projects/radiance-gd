# Sonic Radiance - Overworld

## Présentation générale

L'overworld représente toute les zones où les personnages se déplacent librement, et peuvent agir suivant leur capacités pour accéder aux différentes zones, etc. Il a comme objectif d'avoir un fort aspect "plateformer", et est inspiré à la fois des Sonic Adventure et des Mario & Luigi.

Le but est de "casser" l'aspect ennuyant que peut avoir les overworld classique avec les capacités traditionnels d'un Sonic.

## Esthétique

Le style de l'Overworld est inspiré de Sonic Battle, et cherche avant tout à être cohérant avec le reste du jeu dont les combats et mini-jeux (qui utiliseront tous ce genre de style)

Vous êtes dans un espace en vue semi-topdown à la manière d'un RPG-classique, mais avec les sprites de sidescroller de Sonic (en vue de côté), qui donc à la manière d'un Paper Mario vont surtout regarder dans deux directions.

Les ennemis seront visibles sur la carte, et certains dangers pourront affaiblir les personnages voir les mettres KO où leur faire subir des altérations d'état. Les gimmicks traditionnels d'un Sonic seront également intégré à l'overworld : vous pourrez y grinder, sauter sur des ressorts, collecter des rings, etc.

Ils pourront se déplacer librement et non en case par case, et auront accès à tout un éventail de capacités (voir dossier des personnages), qui permettront petit à petit d'accéder à de nouveaux endroits de l'île.

## Les deux types "d'overworld"

Il est composé d'un seul gameplay, mais de deux types de "zones" :

- Les Adventure Field sont les espaces d'explorations principaux, plus libre. Ils peuvent être des villages, de grands espaces, etc. Ils essaient au maximum d'éviter d'être linéaire, et contiennent pas mal de secrets.

- Les "Action Stage" sont des endroits plus réduits, fonctionnant un peu comme des niveaux pouvant avoir deux buts : trouver un moyen d'accès à une autre zone (à la manière de la rivière Zora dans Zelda OoT : ce sont des endroits à traverser jusqu'au bout pour accéder quelque part) où être des sortes de "donjons". Les actions stages sont donc souvent plus linéaire que l'overworld et avec plus de "puzzle" afin d'offrir du challenge aux joueurs.

*( TODO: indiquer les différents Adventure Field et Action Stage dans le fichier des zones et stages )*
