# Sonic Radiance - Direction et Management de Monado Prime

## Gabriel Seraph (directeur de Monado Prime)

- **Espèce :** Humain

- **Age :** 52 ans.

- **Description Physique :** Gabriel Seraph est un grand homme, qui peut parraitre aux premiers abords imposant. Il porte constamment un costume avec son insigne de commandement de Monado One. Il porte de petite lunette rectangulaire, et ses cheveux grisonnant sont rabattu en arrière.

- **Histoire et description psychologique :** Gabriel Seraph est le nouveau chef de la base depuis l'incident d'il y a 12 ans, avant quoi il était le second d'Ys Gradlon dans la direction de la base. Si son élection a été unanime, il est souvent vu comme un couard imbécile et inepte, incapable d'arriver à la cheville de son prédécesseur. Il est mal vu à cause de principalement deux évévenements : son incapacité à dire ce qui s'était exactement passé lors de l'incident qui a tué Ys Gradlon ainsi que de nombreux ingénieurs, et le fait d'avoir accepté l'offre de HexaECO (compagnie rivale d'énerige de Rimlight) pour obtenir de l'énergie dans la base.

Celui-ci accorde cependant peu d'importance à son image, estimant que "tout l'honneur de ce cher conseil d'administration n'aurait qu'affamé la base". Il n'hésitera pas cependant à critiquer publiquement ses adversaires sur leurs affirmations, rappellant que les centrales chaotiques ne permettent pas la construction d'une nouvelle après incident.. Cela lui permettra de perdre une partie de ses adversaires, lui assurant peu d'ennemis dans son propre camp pendant les les premières année de son mandat.

Cependant, il recommence à être raillé depuis l'arrivée de badnics du Dr. Robotnik, pour son incapacité à agir. L'appel de Sonic, Tails et Amy sera la culmination de cela, et aujourd'hui son mandat est très mal vu par ses adversaires, qui appellent à sa démission.

### Belissama (dirigeante de la phytosphère)

- **Espèce :** Chèvre

- **Age :** 34 ans

- **Description Physique :** Belissama est une grande mobienne, avec une forte carrure. Son pelage est du même blanc neige teinté de marron que ses deux enfants, mais avec deux immenses cornes partant sur les côtés. Belissama porte généralement un uniforme de la base, faisant partie de la classe dirigeante

- **Histoire et description psychologique :** Fille de l'ancienne Grande Prêtresse du peuple Sidh *Morrigan*, et mère de Taranis et Anrad, Belissama est une femme rude et autoritaire, qui s'occupe du bon fonctionnement politique de la phytosphère, et notamment des nombreux anciens membres du peuple Sidh qui s'y trouve. Elle était destinée à devenir la nouvelle Grande Prêtresse, mais a dû reprendre le rôle de son père depuis la mort de son frère, son époux et ses deux parents lors de l'incident d'il y a douze ans.

Devant à la fois s'occuper de ses enfants et d'un village entier, tout en faisant le deuil de tout ses proches, Belissama a décidé à ce moment au grand déplaisir des autres anciens membres du peuple Sidh de ne pas reprendre aussi le rôle de prêtresse, estimant que la vie de son peuple et de sa famille était plus important que les légendes de son peuple. Cependant, elle laissa à Anrad la possibilité d'apprendre sur le rôle de prétresse quand celle-ci affirma son intérêt pour. Elle déclare aujourd'hui toujours que même si prendre le rôle de prêtresse aurait sans doute permis d'aider face à la *maladie*, tout est mieux que si elle avait tenté de tout faire à la fois.

Son mandat ne fut pas facile, puisqu'elle du faire avec les critiques sur le fait qu'elle n'avait pas accordé plus d'attention à son rôle de prêtresse, les doutes sur sa capacité à diriger en ayant des enfants aussi indiscipliné que les siens, l'apprentissage de sa fille, le manque d'intérêt politique pour la vie des anciens du peuple Sidh et la préservation de leur tradition, ainsi que la haine latente entre ceux qui avaient accepter la construction de Monado Prime et ceux qui l'avaient refusé. Elle a entraîné également au combat son fils, étant elle-même capable de se défendre en combat singulier.

Cependant, les choses ont changés aujourd'hui et nombreux sont aujourd'hui ceux qui la respecte pour sa force, son courage ainsi que son écoute envers les scientifique et son équipe. Sa force de caractère et son esprit combatifs lui permirent non seulement de résister à la charge de travail, mais également aux tentatives d'influence et d'intimidation de Robotnik.

Elle fut très déçu quand elle appris que son fils avait travaillé pour Eggman, bien plus qu'en colère.

### Lug (dirigeant de l'hydrosphère)

- **Espèce :** Taureau

- **Age :** 65 ans

- **Description Physique :** Lug est un immense taureau, imposant et charismatique. Sa fourrure est entièrement noire, et il porte fièrement l'uniforme de Monado Prime.

- **Histoire et description psychologique :** Lug est le plus ancien des dirigeant de Monado Prime, et également celui qui a signé le contrat de collaboration entre le Peuple Sidh et le projet Monado il y a plus de 30 ans. Il estime que cette collaboration est importante pour les deux partie, et que le Stygian Rift aurait été plus utile mieux exploité que laissé à l'abandon. Il est l'un des dernier des 12 chefs encore en vie.

Si pas mal de membre du peuple Sidh le voit comme un vénal qui a vendu son peuple pour sa petite vie paisible, il prend très à cœur les conditions de vie de son peuple. Il négocie constamment pour laisser une plus grande place aux traditions du peuple Sidh dans la vie de la base, et lutte activement contre les différence salariale et de niveaux de vies entre les différentes couches sociales de la base.

Malgré les rumeurs, il n'y a pas d'animosité entre lui et Belissama, et les deux réussissent à travailler ensemble sans trop de problème.

Plus accessible et moins "effrayant" que Belissama, il aidera autant que possible les héros dans leur quête contre le Dr. Robotnik.
