# Sonic Radiance - Membres du peuple Sidh

## Morrigan

L'ancienne prétresse du Stygian Rift et la grand-mère de Taranis et Anrad. Sorcière puissante et presque légendaire, elle est encore admiré voir crainte malgré sa mort il y a 12 ans lors de l'incident. Ses pouvoirs était tout autant connu que son caractère droit et puissant. On dit qu'elle était même crainte de Bénélos.

## Benelos

L'un des douze chef d'avant la grande lutte. Cet immense et ancien Bison était un guerrier fier et féroce, qui dès que le projet de la Gosth Central à commencé à avoir des problèmes, s'est farouchement opposé à Ys Gradlon. Cependant, si ces instincts guerriers était pour protéger son peuple, c'est lui qui a prononcé le banissement de tout ceux ayant aidé le projet de centrale, et qui a lancé la bataille finale ayant provoqué l'embalement des problèmes.

Il est vu au village principal comme un grand héros, et tout ceux remettant en doute ses agissements sont généralement mal vu.

## Dahut

Une brebis, mère d'Ankou et ancienne scientifique ayant travaillé sur le projet Millenium. Docteure en énergie chaotique optimiste sur le fait que les sciences aideraient le monde à devenir meilleur, elle a participé au projet dès qu'elle à sû que le conglomérat était intéressé par le Stygian Rift. Elle est celle qui a convaincu une partie des douze chef d'accepter le projet. Elle est morte lors de l'incident. À cause de cela, elle est considérée comme coupable de toute ce qui s'est passé durant ce projet, et son fils unique Ankou subit les contrecoups de cette réputation.
