# Sonic Radiance - Personnages liés à GUN ou aux FUs

- **Ys Gradlon** est l'ancien commander des armées du GUN, disparu il y a de cela 12 ans lors de l'incident de Gosth Island.

## Les Chaos Breakers

> Les Chaos Breaker sont un groupe para-militaire venant essentiellement de GUN, qui sont particulièrement inquiet à cause des pouvoirs développés par les hybrides. Ils ont peur que cette différence de puissance fasse tomber leur peuple en position de faiblesse.
