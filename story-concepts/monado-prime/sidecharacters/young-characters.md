# Sonic Radiance - Personnages jeunes et adolescents

## Taranis

- **Espèce :** Bouc

- **Age :** 14 ans.

- **Description Physique :** Taranis est un jeune bouc blanc, dôté de gants et chaussure style "cuir"/"aventurier". Il porte toujours une sorte de bateau d'acier, où il peut faire transiter de l'électricité.

- **Histoire et description psychologique :** Taranis est l'un des descendant du peuple Sidh de la base. Vivant dans une cabane située dans les bois de la pythosphère, c'est un adolescent colérique, plein de rage contre les injustices qui existent dans la base. Il faut dire, sa mère était fille d'un des 12 chefs du peuple Sidh, et est l'une des dernière personne qui tente tant bien que mal de maintenir une forme d'équilibre chez les descendants du peuple Sidh dans la base.

Il travaille secrètement pour l'Eggman Empire, ayant été amené par Eggman suite à diverse action positive que celui-ci à fait pour quelques membres du peuples sidh, tel qu'attaquer des créantiers, où protéger secrètement une de leur cachette dans la phytosphère. Il n'est pas vraiment au courant des plans du docteur, mais veut lui payer sa dette, et n'a pas grand chose à faire des conséquences des actions d'Eggman pour le reste des Fédérations Unis où pour la ville principale.

Taranis à de ce fait une vision assez sombre du monde, furieux de l'injustice qu'à subit son peuple, et de l'expropriation de leur propre territoire. Pour lui, tout cela est de la faute des constructeurs de la base. Il a une animosité pour toute personne travaillant pour le projet, jeune ou adulte, et surtout pour Ankou, dont la mère s'occupait du design de la Gosth Central. Cependant, il fait partie des rares personnes ne pensant pas qu'Ankou est un poison *en lui-même*. Il est juste pour lui le fils d'une traitresse au peuple Sidh.

Il a énormément de mal à faire confiance aux autres, mais à une certaine attirance qu'il essaie de combattre pour les idéaux de Sonic. Est-il bien possible de sauver tout le monde, existe-t-il une solution pour aider son peuple sans faire de mal à qui que ce soit ? Il aimerait y croire, mais n'y arrive pas.

## Ankou

- **Espèce :** Mouton

- **Age :** 13 ans.

- **Description Physique :** Un petit mouton noir un peu chétif, le jeune Ankou se remarque surtout par son grand médaillon, et la grande cape qui l'entoure entièrement, comme pour le protéger. Il a de toute petite corne qui dépassent à peine du sommet de son crane.

- **Histoire et description psychologique :** Craintif, peu loquace et encore moins accessible, Ankou est un jeune orphelin qui se cache une grande partie du temps dans les zones inutilisée où "technique" de la base. Il connait tout les passages secrets de Monado Prime et les utilise pour s'éloigner des autres. Il a d'ailleurs dans un mur de la base l'endroit où il vit, une sorte de zone de passage de tuyant avec dedans un hamac volé et de quelques magazines récupéré à droite à gauche.

Ankou est comme il est à cause des brimades et du harcèlement qu'il a subit de ses camarades depuis qu'il est tout jeune. Il est le fils de Dahut, l'ingénieure de la Gosth Central, et si peu de gens connaissent exactement ce qu'elle était, ils le voit comme le fils de la cause de l'incident d'il y a 12 ans. Certains, moins au fait, croient qu'il est la cause du "poison", un mystérieux phénomène ayant conduit à la fermeture de plusieurs endroits. Surnommé carrément "le Poison", il a donc constamment été rejeté.

Très défaitiste et avec une grande tendance à se déprécier (voir à se haïr), Ankou estime qu'il ne mérite pas spécialement d'être "sauvé". Il a constamment l'impression que vu son ascendance, il devrait plus "laisser les autres tranquille" et vivre dans son coin. Il a du mal à accepter les preuves d'amitié. Il n'essaie à cause de cela même pas de se défendre, et se laisse tristement faire quand on l'attaque. Ankou est non-combattant, et n'aime pas du tout l'utilisation de la force.

Anrad est l'une des rares personnes à faire preuve de gentillesse avec lui, et paradoxalement il trouve Taranis moins pire que les autres, parce que même s'il sait parfaitement que le jeune Bouc le hait, le bouc n'irait jamais s'attaquer à plus faible, et ne le considère pas comme la source du poison. Cependant, Taranis n'est pas tendre non plus, et rejète constamment Ankou quand il tente de l'approcher.

## Anrad

- **Espèce :** Chèvre

- **Age :** 16 ans.

- **Description Physique :**  Une chèvre plutôt de grande taille, avec des cornes imposante, et une grande tenue de barde. (Grand chapeau, vêtements type "voyageur ancien").

- **Histoire et description psychologique :** Anrad est une jeune barde, qui fait souvent tâche dans la station. Beaucoup de gens la trouve bizarre, voir effrayante, ce malgré un caractère plutôt jovial et accessible. C'est que, dénuée de toute honte de ce qu'elle est, d'envie de correspondre aux normes sociales ou de discretion, la chèvre n'hésite pas du tout à aller voir les passant pour leur raconter les chants anciens du peuple Sidh, où à faire des grands discours sur la place public.

Les forces de l'ordre elles-mêmes souvent hésitent un peu à la déloger, de peur qu'elle aurait quelques pouvoirs mystiques Sidh caché, surtout que malgré qu'elle fasse un peu flipper, elle est plutôt appréciée des gens. Si les rumeurs disent que ses pouvoirs sont formidable, elle ne semble pas se battre. Elle a également une formidable tendance à troller, étant très forte pour remarquer les paradoxes chez les autres comme chez elle-même. 

Après, ses actions ne sont pas que de l'excentrisme. Bien plus sage et réfléchie que son frère, et même que le jeune Ankou, elle est gardienne des anciens chants de la civilisation Sidh. Elle a refusée d’aider Eggman dans sa quête, mais d'un autre côté n'a rien révélé du rôle de son frère dans les plans d'Eggman aux autorités. Elle forme une sorte de troisième camps entre Eggman et la direction de Monado Prime. Elle suivra les héros plusieurs fois, intéressée par leurs objectif, et voulant sauver son frère des griffes d'Eggman.

Elle s'en veut secrêtement de son échec à avoir gardé son frère dans la lumière.
