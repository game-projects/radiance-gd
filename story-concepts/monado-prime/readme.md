# Sonic Radiance - Univers et Histoire

Dans ce dossier se trouvent les différents documents sur les concepts, le gameplay, l'univers et l'histoire de Sonic Radiance. Se passant dans une biosphere artificielle isolée et présentant une aventure inspiré des Adventure, le jeu possède des éléments de lore conçu pour l'histoire-même, mais lié plus ou moins au reste du lore.

## Synopsis

L'histoire de Sonic Radiance se passe dans la base de Monado Prime, une biosphere artificielle en circuit fermé habitée conçue en partenariat entre les Fédérations Unis, G.U.N. et l'entreprise Rimlight. Cette base est construite sur l'île rocheuse de Fortitude Rock, une île quasi-désert située dans les zones polaires. Présentant de nombreux espaces de vies et biomes artificiels, la base est considérée comme l'example à suivre pour les futurs conquêtes spatiales. La base est autonome sauf en lumière (elle a besoin de la lumière du soleil pour faire pousser les plantes), c'est à dire qu'elle est capable de produire elle même tout ce dont elle a besoin en terme d'eau, nourriture et air.


Sonic, Tails et Amy (équipe de début de jeu) arrivent sur l'île suite à un appel du *Directeur de Monado Prime*, qui est inquiêt de l'arrivée de robot d'Eggman dans certains biomes de l'île. Il a peur que le docteur tente de s'emparer de la base et d'en faire un outil de conquête à la MeteorTech. Il a alors demandé de l'aide aux trois mobiens pour qu'ils se débarrassent de la présence du savant fou dans la biosphère.

Cependant, la base recèle bien des mystères, et Sonic & compagnies comprendront bien vite qu'on ne peut comprendre la menace présente que subit la biosphère, sans découvrir tout son passé...

## Fonctionnement

L'objectif de l'organisation de l'histoire est de trouver une manière efficace de combiner les forces d'un Sonic Adventure et celles d'un RPG. Cependant, comme tout les personnages principaux peuvent faire partie de la même équipe, plutôt qu'une liste d'histoire à faire en parallèle, l'idée serait plutôt de jouer sur les différents types d'histoire.

Le principe de l'histoire finale qu'on peut obtenir après avoir fait les autres histoire peut être gardé grâce au système de quête : après avoir terminé les quêtes d'importance principale, on peut passer aux quêtes secondaires.

Cela donne les types de quête suivantes :

- Deux types de *quêtes principales* : Ces quêtes sont celles qui permettent de débloquer la final story, et sont divisé en deux types : une progression principale plus ou moins linéaire (disons que parfois deux quêtes "linéaires" seront accessible en même temps et devront êtes faites toutes deux pour avoir la suite), et des arcs de quêtes d'importance principales mais servant ensuite à pouvoir déclencher la *final story* une fois que toutes les quêtes principales sont terminées.

- Les *quêtes de soutiens* : des quêtes secondaires à l'intrigue, formant des arcs où des quêtes seules, permettant de faire avancer plus les relations entre personnages et leur personnalités.

Cela fait que certaines quêtes auront des personnages requis pour être déclenchées. De plus, à cela pourra se rajouter des petites saynètes/scénette déclenchable si certains personnages sont présents à certains endroits et lieu, et qui serviront surtout à amuser où faire montrer des points de vue différent sur les personnages.
