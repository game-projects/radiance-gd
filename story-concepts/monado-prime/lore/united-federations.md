# Sonic Radiance - Les Fédérations Unis

Les Fédérations Unis sont une république parlementaire avec un régime présidentiel. Puissante nation, elle a subit cependant plusieurs crise depuis les attaques du Docteur *Ivo « Eggman » Robotnik*.

Elles peuvent être vu comme quelque chose entres les Nations-Unis et les États-Unis. Le but de cet état est de garantir un développement tout en respectant les libertés des pays qui en font partie (et qui conserve une certaine indépendance). L'île de Sunlit Island est une colonie des Fédérations Unis, avec comme représentant le maire de la ville de *Sunlit Metropolis*.

Leur technologie est majoritairement contemporaire, mais a réussi de grande avancée depuis 50 ans à l'aide des technologies chaotique, en grande partie découvertes par le professeur Gérald

Dans le jeu, les Fédérations Unis, n'ont pas une présence très forte dans le jeu, en tout cas pas directement.

## Les Gardian Units of Nations

Les Gardian Units of Nations (ou G.U.N. parfois orthographié en GUN) sont la principale force militaire des Fédérations Unis, et ont comme rôle de protéger à la fois les Fédérations Unis, et à la fois les pays alliés. Cette armée est dirigé par un Commandant.

L'armée est actuellement en grande partie dédiée à affronter le Dr. Ivo Robotnik, qui vise à conquérir le monde, mais n’arrive pas à le localiser et à lancer des attaques précises. La capacité à disparaître de ce savant étant déconcertante, et ses moyens encore inconnus. L'inefficacité de GUN à vaincre les menances "surnaturelles" est la source d'une méfiance chez certaines personnes.
