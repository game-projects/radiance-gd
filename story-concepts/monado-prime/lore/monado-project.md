# Sonic Radiance - Le "Projet Monado"

Le projet Monado est le projet qui a entrainé la fondation de la base de Monado Prime, et de possible futures autre base de type Monado. Il s'agit d'un projet ambitieux créé par plusieurs grandes acteurs des Fédérations Unis, et qui a eu un effet considérable sur le développement technologique de la fédération.

Note : ce document ne contient que l'histoire globale du projet, pour les éléments précis de backstory, le fichier backstory.md sera plus votre ami :)

## Les bases Monado

Les bases types « Monado » sont des biosphères artificielles utilisant l'activité bactérienne pour créer un système écologique pro. L'idée est de créer un espace qui pourrait être hermétiquement fermé et vivre sans besoin d'apport extérieur. Des usines bactérienne s'occupent du plus gros de recyclage de l'air et des déchets biologiques, permettant de produire à la fois de l'oxygènes et des engrais vert pour les espaces de cultures.

Ces bases peuvent abriter plusieurs centaines de milliers de personnes, et sont capable de vivre en autarcie presque totale, les deux éléments les plus important à leur survies étant : du soleil (pour la survie des biomes artificiels) et une sorte d'énergie performante (le plus souvent des Chaos Central). Le but est d'à la fois construire une base de recherche, une expérimentation dédiée à la future colonisation spatiale,

Le projet Monado a été débutée il y a environ 30 ans, sur l'impulsion du gouvernement des Fédérations Unis, et est supervisé par l'entreprise énergétique Rimlight (produisant les Chaos Drives et les Chaos Centrales) ainsi que par plusieurs équipes universitaires. Les Fédérations Unis avaient pour but d’offrir un monde meilleurs à tout les habitants de la planète, et empêcher à nouveau les guerres d’intervenir. Et le but de ces bases étaient d’illustrer la meilleure vie qu’ils auraient tous, ainsi que de préparer une future expansion spatiale.

## La construction de Monado Prime

Monado Prime est la première base de type monado construite et terminée. Elle a été ouverte officiellement il y a de cela 26 ans, après 7 ans de construction, sur l'île de Fortitude Rock, habité seulement par un peuple de mobiens ongulés, le peuple Sidh. Ce site a été choisi au début du projet, il y a 35 ans par les scientifiques de Rimlight, fortement intéressé par la présence d'une faille chaotique extraordinaire, le Gosth Rift.

Après de dure négociations avec le peuple Sidh, ils purent construire la base, ansi que la Gosth Centrale, centrale chaotique de toute nouvelle génération.

Le projet continua sa vie normalement, s'améliorant et permettant de nombreuses découvertes, jusqu'à il y a 12 ans, un mystérieux incident arrivant dans la couche inférieur de la base, la Gosth Central. Personne ne sait exactement ce qui s'est passé, et l'incident est maintenant oublié de la plupars des nouvelles personnes travaillant dans la base, mais depuis, la base doit se fournir en énergie via une centrale affilliée construite par HexaECO. Un biome a également été interdit au public depuis cet incident.

## La base aujourd'hui

Aujourd'hui, la base est une cité semi-indépendante dirigée par un commité scientifique et un conseil de citoyen, qui tourne un peu plus au ralenti qu'à l'époque et n'a jamais atteint le nombre d'habitant révé par ses créateurs trop optimistes. Si elle a été amélioré et modernisée avec le temps - bien qu'elle était très en avance sur son temps - elle produit moins de découverte et d'évolution. Cependant, sa réputation reste d'être le futur des Fédérations Unis, et elle est un franc succès sur le plan de l'autarcie.
