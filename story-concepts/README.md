# Sonic Radiance - Univers et Histoire

Dans ce dossier se trouvent les différents documents sur les concepts, le gameplay, l'univers et l'histoire de Sonic Radiance. Deux concepts différents d'histoire et de types d'univers sont encore en réflexion, pour en choisir un pour le jeu final.

*TODO* : Tenter de concillier les deux concepts ?

## Monado Prime

Ce concept mélange des aspect Metroid (notamment le fait de se dérouler entièrement dans une biosphère artificielle) avec ceux d'un Sonic Adventure.

Le principal défaut de ce concept là, est que l'aspect "mystique"/"ruines" peut sembler être un ajout de dernière minute ? Ou alors il faudrait mieux l'établir...

## Sonic Advance RPG

Ce concept est un concept plus proche des Sonic Advance (notamment Advance 3, avec un peu de Battle), et des jeux typé RPG qui sortait sur la GBA/DS (genre les Pokémon de Hoenn et de Sinnoh, Pokemon Mystery Dungeon)

Dans ce concept, l'histoire se déroule sur Radiant Island, une île paradisiaque ou divers problèmes sont arrivés. Sonic et ses amis vont devoir tout autant comprendre les mystères de l'îles.

*TODO:* Abandonner le concept "tropical" de l'île, voir s'inspirer d'un aspect plus celtique, et de la région de Sinnoh dans Pokémon ?
