# Sonic Radiance - Main Story - Act 1 : Des robots partouts

## Synopsis

Sonic, Tails et Amy ont été appellée sur l'ile de Sunlit Island par le maire de l'ile. L'ile fait en effet face à des attaques ponctuelles de Badnics, mais sans qu'Eggman ait fait la moindre annonce ou menace. Les trois héros arrivent donc sur l'île, et vont d'abord devoir explorer la ville principale (Sunlit Metropolis) pour trouver le maire, et avoir leur mission.

Il apprendront que les badnics attaquent en ce moment un petit village touristique du sud de l'île, le Merchant Village. Les trois héros devront se diriger dans ce village, afin d'aller sauver les habitants.

Pour cela, ils vont devoir tout d'abord traverser les plages, qui sont infestées de badnics. Ce sera l'occasion d'avoir les premiers combats expliquant le système de combat. Traversant la plage jusqu'au village, ils vont trouver un village attaqué par les robots d'Eggman, qui cherchent *un mystérieux médaillon*. Le garçon qui le possède, ainsi que certains autres jeunes du village, vont fuir jusqu'à la crique à l'autre bout de la plage. Les héros devront aller les chercher.

Le groupe de cinq ados se trouve dans le premier "mini-donjon". Les personnages y utiliseront leur première capacité, le saut, montrant le mélange zelda+plateformer de l'overworld et des donjons. A la fin de ce mini-donjon, les personnages découvriront le premier boss, un Egg-Robo. Cependant, après l'avoir vaincu, ils ne réussiront à sauver que quatre des cinq enfant. Ankou, *l'enfant au médaillon* se fera capturer par un autre EggRobo, qui l'aménera à un endroit inconnu.

Les héros devront alors retrouver le jeune Ankou, mais découvriront que cela ne sera pas forcément facile...

## Éléments importants

- 2 ville : Sunlit Metropolis (peut-être changer le nom pour faire référence aux niveaux tirés de Shadow Boost ?)

- 1 zone de l'overworld (la plage)

- 1 donjon (le bosquet)
