# Sonic Radiance - Main Story - Act 2 : Rescue Mission

## Synopsis

Après l’enlèvement d’Ankou, les autres personnages le cherchent et essaie de le retrouver, se demandant pourquoi cet évenement s’est produit. Les personnages devinent facilement que le jeune garçon a du être emmener dans une base construite par Eggman.

pendant les recherches, ils découvrent que sa mère, Dahut, n’est pas apprécié par les deux camps. Amy va s’énerver contre des gens en disant que s’attaquer à un enfant de 10 ans parce qu’ils ont des soucis avec sa défunte mère, c’est non seulement idiot mais lache.

Découverte qu’elle a participé à un projet avec les humains, le projet Gosth Island, et qu’elle est considéré comme la cause du backfire du projet, faisant disparaitre de nombreux membres du projet, de nombreux membres du peuple Sidh et le général de gun de l’époque, Ys Gradlon.

Ils finissent par obtenir l’emplacement de la base d’Eggman. Ils vont y retrouver le petit, mais Eggman a le médaillon et compte l’utiliser pour découvrir tous les secrets du projet. Ils sera accomapgné de Taranis, son nouveau lieutenant qui est un membre de l’autre tribue du peuple Sidh. Le premier combat visera à vaincre les deux en équipe.

Ils ne découvrent qu’une chose, c’est qu’Eggman s’intéressent aux différentes répercussions du projet Gosth Island.

Apparition de Rouge, qui espionnait dans la base d’Eggman. Ce serait elle qui donnerait accès aux sources d’informations sur le projet Gosth Island aux héros, puor leur recherche d’information. L’espionne indique qu’ils leur faudraient aller obtenir des informations des deux grands groupes liés au projet : Rimlight et G.U.N.
