# Sonic Radiance - Main Story - Act 6 : Blackout

## Synopsis

Une attaque a été lancé sur la centrale chaotique de l'île, par Metal Sonic et les héros. Il va être temps d'entrer dans le cœur du projet utopique des humains, et de voir quelle était cette fameuse ville utopique du futur que le projet Millenium voulait construire.

Les héros vont devoir traverser toute la montagne, trouver l'entrée vers la base qui se trouve en haut. En haut, ils devront passer les différents endroit qu'elle contient, jusqu'à se battre contre Metal Sonic. A l'intérieur, ils découvriront à quel point le projet était important pour ses membres, et leur croyance à un futur meilleurs. Ils découvriront également les derniers messages de ceux qui ont subit l'incident.

 Une fois en haut, ils apprendront que c'était un piège, et qu'Eggman attaque la ville, pour la bannir.

 Ils n'arriveront pas à temps.

 Toute la ville sera engloutie dans le Stygian Realm.
