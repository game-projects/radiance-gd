# Sonic Radiance - Main Story - Act 2 : Le Projet "Gosth Island"

## Synopsis

Ce chapitre se passe en deux partie : tout d’abord, les héros vont devoir aller chercher dans les archives sous-marine de l’île des informations sur le projet Gosth Island.

Ensuite, ils vont devoir se rendre jusqu’à l’administration du site Rimlight de l’île. Cela leur demande de passer à travers la Diamound Higway, une voie rapide qui traverse les montagnes jusqu’au site isolé ou se trouve les QGs de Rimlight. Sur la voie, ils pourront obtenir le pouvoir de tirer, permettant d’utiliser Rouge ou Tails pour traverser la voie. Une fois cette voie traversée, il vont devoir se rendre jusqu’au QG, et vont devoir traverser quelques étages garder pour rencontrer le directeur. Celui-ci leur expliquera le fiasco du projet, mais dit qu’il n’était pas présent à l’époque, et que seul G.U.N. possède des archives sur le sujet.

- Passage dans les archive du G.U.N. pour en savoir plus sur le projet → Rencontre avec Shadow ? + Obtention du Sceau limitant temporairement le pouvoir du Styx. Rapport qqpart avec Ankou, qui voudra en savoir plus sur sa mère ?

- Affrontement contre l’Egg Charon, le premier robot utilisant le Styx tout particulièrement. Le combat sera difficile, et ne sera possible que par l’utilisation du sceau qui bloquera ses effets. Cependant, il sera consummé. Eggman dit qu’ils n’auront pas autante de chance la prochaine fois, après. Il annonce qu’avec sa nouvelle machine, le Egg XXXX, il aura le pouvoir de bannir des villes entières dans « la dimension d’origine de cette merveilleuse énergie ».

Maintenant qu’ils connaissent les origines de tout ce plan, ils savent à qui ils doivent parler pour trouver une manière de lutter contre le Styx : Au peuple Sidh.
