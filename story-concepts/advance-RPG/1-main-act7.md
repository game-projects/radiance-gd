# Sonic Radiance - Main Story - Act 7 : L'Assault Final

## Synopsis

Toute l'équipe des héros va mener un assault sur l'île de Gosth Island. Le scénario commence par une attaque en Tornado, avant de passer sur une exploration de l'île. (L'extérieur de l'île et l'intérieur forment un seul grand donjon, un peu à la manière du chateau d'Hyrule de BotW).

Lors de l'exploration, divers logs du projet sont présent, ce qui permer de découvrir (du point de vue de ceux qui était pour) comment Ys Gradlon a progressivement sombré dans la paranoïa.

Le donjon contiendra au moins deux mini-boss, et son boss sera Taranis. Cependant, alors qu'on se préparera à attaquer Eggman, celui-ci activera son plan.

Les héros sont alors banni dans le Stygian Realm
