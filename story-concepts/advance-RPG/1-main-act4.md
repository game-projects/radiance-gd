# Sonic Radiance - Main Story - Act 4 : Le Peuple Sidh

## Synopsis

Arrivée dans chez le peuple Sidh, après traversée des zones anciennes.

Le peuple Sidh expliquera qu’un mystérieux étranger en quête de purification les a déjà prévenu du danger d’Eggman, et qu’il était manipulateur et doué pour mettre en confiance. Cependant, la cheffe expliquera qu’ils n’ont simplement pas le choix : ils manquent de nourriture et de biens, et seul l’Empire d’Eggman a accepté de leur fournir ce dont ils ont besoin. Elle explique qu’elle n’a aucune confiance en Eggman, mais qu’elle doit penser avant tout à ce que son peuple mange à sa fin. Elle dit qu’elle regrette que son fils soit autant certains qu’ils retrouveront leur gloire passée par ces moyens.

- Lorsque nos héros demanderont à rentrer dans le Canyon pour pouvoir découvrir le secret du Styx et de comment le contrer.

- Trois fragments d’artefact à récupérer, chacun comptant une partie de l’histoire du peuple. Retraversée de lieux pour récupérer les artefacts. Cela permet de comprendre pourquoi est aussi

- Dans le désert, passage de l’épreuve de la justice, qui prouvera au peuple qu’on est digne de confiance. Durant cette épreuve, rencontre de Knuckles. En fait, c’est limite plus la récupération de Knuckles qui recevra la confiance de Belisama. Celui-ci acceptera de témoigner pour eux.

- Ils peuvent aller dans l’ordre, et découvrent donc le pouvoir originel du Styx, la corruption et le banissement. Ils découvrent à quel point ce monde corrompu est dangereux. Rencontre avec Anrad, la sœur de Taranis. Elle explique les éléments sur comment fonctionne le Styx, le fait que le Styx ne cherche que deux chose : s’étendre et dévorer. Si Eggman réussi a utiliser le Sytigian Rift pour bannir des villes entières, le Styx s’étendra comme jamais. Il envahira tout : La ME, les Chaos Emerald et les Chaos Vein, jusqu’à ce qu’il n’y ait de Chaos Energy que le Styx.

Tails remarquera que l’action d’Eggman condamne tout. Seul ses technologies, basées surtout sur l’énergie vitale, vont survivre, tandis que les centrales Chaotiques toutes seront inutilisables. Knuckles explique que de nombreux artéfact anciens seront aussi affecté, ainsi que de nombreux utilisateurs d’énergie chaotique aussi. Tout le monde est affecté par cette menace. Remarque que Shadow serait sans doute le premier corrompu.

Les héros comprennent alors quelles sont les trois menaces du plan TRIPLE THREAT :

- Banissement de villes entières, augmentant la présence d’Energie Vitale dans le Styx

- La présence de toute cette énergie vitale va produire un emballement du Styx, corrompant en grande partie l’énergie chaotique, provoquant les effets cités plus haut.

- Invasion de Badnics attaquant alors les zones affaiblies ayant perdu une grande partie de leurs forces militaires, de production et énergétique.

Tout le monde serais mis en danger par cette attaque. Les héros et Anrad se mettent d’acord pour dire qu’il faudra intégrer tout le monde dans une telle lutte, au vue de l’avance qu’à Eggman : il contrôle déjà de nombreux points de l’île, a des agents partout.
