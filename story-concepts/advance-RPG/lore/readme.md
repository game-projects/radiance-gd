# Sonic Radiance - Lore

Le Lore de Sonic Radiance contiendra principalement deux éléments : des éléments qui sont spécifiques au jeu, à savoir des éléments sur l'histoire du jeu et la backstory de l'île ou se produit du jeu, et des éléments qui seront spécifique à la vision du monde de Sonic qui est développé dans le jeu.

Le but de ce fichier est de présenter quelques uns des éléments de base qui constituent ce lore :

- **Un jeu centré autour d'un lieu en particulier** : Comme SA (et SA2 dans une moindre mesure ?), le jeu se situe autour d'une histoire bien particulière qui aura des répercutions sur le "général". L'histoire sera situé autour de l'île de Sunlit Island, et le lore sera donc adapté autour de cet histoire.

- **Ambiance typé 2000** : Ce jeu est également un hommage à toute une époque. L'ambiance sera donc à la fois typé Adventure, mais surtout un hommage à tous les "petit Sonic" sorti à cette époque (Advance, Battle, et les jeux mobiles). En découlera une ambiance qui sera unique, et des références à des plot-lines oubliées depuis un moment. Cependant, cela ne veut pas dire que *rien* des jeux récent ne sera adapté ! Certains éléments considérés comme pertinant pour l'histoire ou le gameplay seront présent. Idem pour les éléments du "monde classic", certains seront adapté.

- **Two-World** : Ce jeu utilise le two-world, parce qu'une des sous-intrigue est fondée dessus.
