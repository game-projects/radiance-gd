# Sonic Radiance - Peuple Sidth

Le peuple Sidh est un peuple de Mobien hétéroclite qui vit sur l'île de Sunlit Island. Ayant autrefois occupé toute l'île, ainsi que la petite île au large de Gosth Island, ils ne vivent aujourd'hui que dans le XXXXXX Village, suité dans les plaines de XXXXX.

Plus qu'un peuple basé sur une race (genre les échidnés), ils sont plus réunis par une culture. Basée sur une culture celtique (notamment les mythologies Bretonne, Irlandaise, Galloise et Ecossaise), les personnages de ce peuple ne sont pas nommés suivant la base normale de Sonic (des mots anglais), mais selon des termes de la culture visée (ici des dieux ou mots celtes), un peu comme Tikal et Pachacamac ont des noms basé sur des temples améridiens.

Leur rôle dans l'intrigue sera un peu ambigüe, notamment à travers les personnages de *Taranis*, *Ankou* ou *Anrad*. Leur nom vient du nom du *Sidh*, royaume des morts chez les Celtes.

## Culture et mode de vie

Vivant dans leur village, dans les plaines et juste avant le XXXX (qu'ils protègent des intrus). Ils vivent principalement en autarcie, et ont une grande méfiance pour les humains. Ils accepteront même de s'allier avec Eggman en échange de vivre et de soutiens financier (estimant que la guerre contre Eggman est une affaire d'humain).

Aujourd'hui, ils protègent surtout le Super Warp Ring qui se trouve dans les cavernes au fond de leur vallée.

Panthéïstes, ils croient en la *Grande Mère*, qui est une déesse de la nature qui est présente en chaque endroit en chaque instant. De ce fait, ils vénèrent tout ce qui est surnaturel, et proche du Chaos, la principale des forces naturelles. En cela, ils ont longtemps vénéré le Stygian Rift situé sur l'île de Gosth Island, qu'ils alimentaient en sacrifice jusqu'à ce qu'ils soient exproprié de l'île de Gosth Island, lors de la construction de la Gosth Central

La "faille noire" comme ils l'appelle reste aujourd'hui encore ce qu'ils vénèrent le plus, et cette expropriation est source de leur colère contre les Fédérations Unis.

Leur chef actuel est Lug, père de Taranis et Anrad, qui est prêt à tout ce qu'il faudra pour aider ses enfants et surtout son peuple.

### Le second village

Il existe un second village, se trouvant au niveau des plages du sud. Ce village est constitué des Sidh ayant désiré participé dans le projet Gosth Island et n'ayant pas péri lors de l'incident.

Les villageois du second village (surnommé le village de *Bhàin*) vivent principalement du revenu du tourisme, et sont assez pauvre mais restent attaché à leur indépendance et à leur mode de vie.

Cependant, ils sont beaucoup moins attaché au fait de rester séparer des humains, et sont considéré par leur pairs du village principal comme des traitres.

## Histoire du peuple Sidh

### Les premiers Sidh

Le peuple Sidh sont à l’origine un groupe de mobien hétéroclite qui s’est installé sur l’île, qui était déserte à l’origine, il y a très longtemps par le Super Warp Ring. Suivant un petit groupe de penseurs, les Sages, ils fondèrent une société, visant à vivre suivant leur vertu et leurs idéaux. Fondée sur la pensée et la recherche du bien absolu, ils ont fondé une civilisation surtout centrée sur la discussion et la présence de ces Sages qui devaient conduire les débats et faire évoluer la science.

Tous les sages et les chefs se devaient de passer quatre épreuve, liées aux quatre vertues cardinales (le courage, la tempérence, la justice et la sagesse). Leur but était de créer une civilisation d'être qui auraient atteint l'état de perfection morale. Cependant, dans leur vision, ils avaient également un aspect "pédant". S'ils voulaient aider les autres, ils avaient tendance à voir de haut les peuples "moins intelligents"

Petit à petit, ils sont devenus une civilisation bénévole et qui a eut de grande avancées techniques, à la manière des Chozos de Metroid. Ils découvrirent sur la petite île de Gosth Island au large de leur île principale la *Chaos Vein*, qu'ils commencèrent à étudier. C'est eux qui sont à l'origine de tous les temples et technologies anciennes qui se trouvent sur l'île. Leur but était d'utiliser la technologie de la faille pour améliorer la santé de leur peuple, ainsi que de facilité la vie des différents peuples.

### La chute

Cependant, ce qui a été la source de leur brillance fut celle aussi de leur fin, à cause de la corruption de la Chaos Vein. En effet, vers la fin de l'île, un groupe de sage commença un nouveau projet. Leur but était de créer un pouvoir qui vaincrait la malchance et l'infortune, la maladie et la peur. Leur but n'était pas de "vaincre la mort" ou d'autres choses du genre, mais de créer une énergie qui apporterait à tous santé et bonne fortune.

Mais une chose leur manquait. La puissance. La Chaos Vein leur apportait énormément de puissance, mais ce n'était pas suffisant pour leur plan. Cela provoqua chez les sages qui travaillait sur le plan une dispute. Que faire ? Des tas de possibilités existaient. Faire une arme provoquant méfortune, et aller chercher d'autres failles ? Abandonner le projet et se contenter des améliorations déjà présente ? Leur choix fut tout autre.  

Les Sidh tentèrent de faire évoluer la Chaos Vein afin de pouvoir avoir plus d'énergie. Utilisant un dispositif pour centrer l'énerige de la faille, cette machine s'emballa, provoquant une catastrophe. Cela provoqua l'évolution du Chaos en une autre force, le Styx. Cette énergie corrompu commença a tout envahir, et la faille commença a happer des êtres vivants.

### La période sombre et la reconstruction

Le peuple Sidh, ayant perdu une grande partie de son peuple et de sa civilisation, ne survécu pas en tant que civilisation. Il s'effondra progressivement, et bientôt ne resta de l'ancienne époque qu'un souvenir lointant, celui de l'époque ou le peuple Sidh était "proche des dieux".

Ils commencèrent à craindre et à vénérer la Chaos Vein, désormais devenu le Stygian Rift. Entendant ses murmures, ils le voyaient comme une émanation maléfique de la puissance naturelle. Ils y ont longtemps conduit des sacrifices. Cependant, cela ne fit qu'augmenter la voracité et l'instabilité de la faille, jusqu'à ce qu'ils approchent de l'extermination à cause de cette voracité.

Les quelques sages qui restaient continuèrent à faire les épreuves et à conduire la civilisation, mais ne connaissaient que de moins en moins les origines de leur culture.  Ils s'isolèrent de plus en plus du monde, conscient d'avoir été à l'origine de la naissance d'un danger, et effrayé par ce danger. Ce n'était pas qu'ils ne comprenaient pas ce danger ou qu'ils étaient stupide : ils étaient effrayé, et n'avaient plus le choix.

Ce fut lorsque la civilisation approcha encore de la chute qu'une des sages décida de tenter autre chose *Dana*, toute sa vie, elle étudia et appris de la faille, avec ses collègues, jusqu'à aller affronter la faille pour la calmer. Elle réussi à apaiser la faille, devenant la première des pretresses de la faille.

Petit à petit, les sages et les rituels disparurent, remplacé petit à petit par un conseil, qui prenait les décisions avec le reste du peuple. Le peuple continua à exister, et évolua perdant son accès à ses technologies. L'île de Gosth Island devint une île sacrée. Les prêtres de la faille continuèrent à exister, et cet état de fait évolua, jusqu'à ce qu'arrive le *projet Millienium* il y a quinze ans.

En effet, l'incident du projet Millienium provoqua le décès d'une partie du conseil, et la perte de Gosth Island qui devint une île interdite, protégée militairement par les *GUN*. Il ne resta du conseil plus qu'un des chefs (Lug, le chef actuel), une chef existant à l'état spectral (Morrigan) et une fille de chef supposée vivante mais disparue depuis l'incident de Gosth Island. (Dahut)

Le peuple vit aujourd’hui divisé, dans une certaine précarité, et une partie s’est tourné récemment vers Eggman, lui offrant des connaissance sur le Stygian Rift en échange de nourriture, de médicament et de vengeance pour certains. Les survivants de l'incident ayant été membres du projet furent bannis, et fondèrent un petit village sur la côte, vivant désormais des revenus touristiques.
