# Sonic Radiance - Lore et Histoire

Dans ce dossier se trouvent les différents documents sur le lore et l'histoire de Sonic Radiance. Se passant sur une île isolé (à la manière des jeux classique) et présentant une aventure inspiré des Adventure, le jeu possède des éléments de lore conçu pour l'histoire-même, mais lié plus ou moins au reste du lore.

## Synopsis

L'histoire de Sonic Radiance se passe sur l'île de Sunlit Island, une petite île sous contrôle des Fédérations Unis. Cette île est une île cependant habité par un peuple mobien, le *peuple Sidh*.

Sonic, Tails et Amy (équipe de début de jeu) arrivent sur l'île suite à un appel du *Maire de Sunlit Metropolis*, qui est inquiêt de l'arrivée de robot d'Eggman sur l'île. Il a alors demandé de l'aide aux trois mobiens pour qu'ils se débarrassent de la présence du savant fou.

Cependant, l'île recèle bien des mystères, et Sonic & compagnies comprendront bien vite qu'on ne peut comprendre la menace présente que subit l'île, sans découvrir son passé...

## Fonctionnement

L'objectif de l'organisation de l'histoire est de trouver une manière efficace de combiner les forces d'un Sonic Adventure et celles d'un RPG. Cependant, comme tout les personnages principaux peuvent faire partie de la même équipe, plutôt qu'une liste d'histoire à faire en parallèle, l'idée serait plutôt de jouer sur les différents types d'histoire.

Le principe de l'histoire finale qu'on peut obtenir après avoir fait les autres histoire peut être gardé grâce au système de quête : après avoir terminé les quêtes d'importance principale, on peut passer aux quêtes secondaires.

Cela donne les types de quête suivantes :

- Deux types de *quêtes principales* : Ces quêtes sont celles qui permettent de débloquer la final story, et sont divisé en deux types : une progression principale plus ou moins linéaire (disons que parfois deux quêtes "linéaires" seront accessible en même temps et devront êtes faites toutes deux pour avoir la suite), et des arcs de quêtes d'importance principales mais servant ensuite à pouvoir déclencher la *final story* une fois que toutes les quêtes principales sont terminées.

- Les *quêtes de soutiens* : des quêtes secondaires à l'intrigue, formant des arcs où des quêtes seules, permettant de faire avancer plus les relations entre personnages et leur personnalités.

Cela fait que certaines quêtes auront des personnages requis pour être déclenchées. De plus, à cela pourra se rajouter des petites saynètes/scénette déclenchable si certains personnages sont présents à certains endroits et lieu, et qui serviront surtout à amuser où faire montrer des points de vue différent sur les personnages.
