# Sonic Radiance - Zone

Le jeu entier se passe sur l'ile de Sunlit Island et ses alentours.

Le jeu étant inspiré en grande partie des type Sonic Advance et des jeux mobiles Sonic, le jeu sera comme les Sonic Advance séparé en sept zones. Ces sept zones contiendront les divers donjons, villes, et espaces d'explorations.

Le theme des noms de niveaux essaira d'avoir un theme commun, un peu à la manière de jeux comme Shadow Shoot (ou chaque endroit).

## Prior art

Ici nous allons regardez quelques listes de niveaux de jeux Sonic à sept niveau afin de voir ce qui est possible de faire pour avoir quelque chose d'intéressant.



### Sonic Adventure

- Emerald Coast (plage tropical)

- Windy Valley (canyon ruines)

- Casinopolis (casino)

- Icecap (glace, eau)

- Twinkle Park (fun)

- Speed Highway (ville route)

- Red Mountain (montagne volcan grotte)

- Sky Deck (aerien base eggman)

- Lost World (ruines eau)

- Hot Shelter (aerien base eggman)

- Final Egg (base eggman)

- Sand Hill (desert)

### Sonic Adventure 2

- City Escape / Mission Street (ville)

- Route 101 / Radical Highway / Route 280 (route)

- Wild Canyon / Pumpkin Hill / Sky Rail (canyon halloween)

- Aquatic Mine (eau canyon halloween)

- Dry Lagoon (tropical canyon)

- Weapons Bed / Metal Harbor (base militaire eau)

- Iron Gate / Prison Lane / Security Hall (base militaire)

- Green Forest / White Jungle (jungle)

- Sand Ocean / Hidden  / Pyramid Cave / Death Chamber / Egg Quarters (base eggman desert)

- Eternal Engine / Crazy Gadget / Final Rush / Lost Colony / Cosmic Wall / Final Chase (deathegg espace)

- Mad Space / Meteor Herd (espace)

### Sonic Advance

- Neo Green Hill Zone (plage tropical)

- Secret Base Zone (base eggman)

- Casino Paradise Zone (casino)

- Ice Mountain Zone (glace eau montagne)

- Angel Island Zone (ruines canyon)

- Egg Rocket Zone (base aerien eggman)

- Cosmic Angel Zone (base deathegg espace)

### Sonic Advance 2

- Leaf Forest (foret)

- Hot Crater (volcan)

- Music Plant (fun)

- Ice Paradise (glace)

- Sky Canyon (aerien canyon)

- Techno Base (base virtuel)

- Egg Utopia (base deathegg espace)

### Sonic Advance 3

- Route 99 (route ville)

- Sunset Hill (tropical)

- Ocean Base (base militaire eau)

- Toy Kingdom (fun)

- Twinkle Snow (montagne eau glace)

- Cyber Track (virtuel base)

- Chaos Angel (ruines)

### Shadow Shoot

- Diamound Highway (ville)

- Peridot Tunnel (base militaire eau)

- Jadeite Forest (foret)

- Pearl Mountain (montagne glace)

- Calcite Hill (tropical)

- Carnelian Bridge (route)

- Ametist Castle (chateau ruines)

### Sonic Heroes

- Sea Gate / Seaside Hill / Ocean Palace (tropical plage ruines)

- Grand Metropolis / Power Plant (technologie ville)

- Casino Park / BINGO Highway (casino)

- Rail Canyon / Bullet Station (canyon)

- Frog Forest / Lost Jungle (foret jungle tropical)

- Hang Castle / Mystic Mansion (chateau halloween)

- Egg Fleet / Final Fortress (base aerienne eggman)

### Sonic Rush

- Leaf Storm (foret)

- Water Palace (eau ruines)

- Mirage Road (desert base ruines)

- Night Carnival (fun)

- Huge Crisis (base aerienne militaire)

- Altitude Limit (base eau militaire)

- Dead Line (base deathegg espace)

### Sonic Rush Adventure

- Plant Kingdom (jungle plage tropical)

- Machine Labyrinth (base steampunk)

- Coral Cave (grotte crystal eau)

- Haunted Ship (halloween eau)

- Sky Babylon (ruines canyon)

- Blizzard Peaks (glace eau)

- Pirates' Island (eau base)

## Notes

- Les Zones sont bien plus des "Adventure Field" et devront être plus généraliste que dans un Sonic habituel (les niveaux genre "la base d'Eggman" ou "Le Casino" seront plus des donjons et des lieux spéciaux plutôt que des zones entières)

- Il faudrait sans doute mieux éviter d'avoir des zones trop "gimmicky" (genre le désert)

## Propositions

### Proposition 1

- **Sunlit Metropolis** (City) : la ville principale de l'ile

- **Radiant Coast** (Plage / Côte) : La zone côtière de l'île, avec les palmier, toussah. C'est ici que se trouve le village "touristique" du peuple Sidh.

- **Ancient Cliff** (Ruines, Falaise) : Les anciennes terres du peuple Sidh, où vivent les Sidh vivant encore conformément aux anciennes règles de leur clan. L'endroit contient de nombreuses ruines Sidh.

- **Lush Forest** (foret/cote) : Les forêt de l'ile *peut-être fusionner cette zone avec Radiant Coast histoire de libérer la place pour un autre truc ?*

- **Utopic Mountain** (grotte, neige, magma, "utopie") : La grande montagne qui surplombe l'ile, et qui donne accès aux anciens espaces d'expérimentation du projet Millenium.

- **Military Bridge** (eau, militaire) : Une grande base de GUN avec des archives sous-marine. Cette base forme un pont sécurisé entre Gosth Island et l'ile principale.

- **Gosth Island/Central** (technologie, ile tropicale) : Une ile entièrement transformée en centrale futuriste. Elle est corrompue par l'énergie du Styx.

### Propositions 2

Le but de cette proposition est d'offrir une plus grande diversité que la proposition 1. En découpant les différents niveaux des Sonic Advance en thématiques, on peut remarquer. En combinant les thémtique du jeu, il serait intéressant d'avoir autant que possible un équilibre entre zones naturelles et artificielles.

- Sunlit Metropolis (ville, route) + funpark et casino en niveau "spécifique".

- Ancien Cliff (ruines, canyon, aride) + halloween en niveau "spécifique". Les anciennes plaines du peuple Sidh. Jadis le cœur de la civilisation, aujourd'hui surtout un endroit sec, aride et où peu pousse.

- Radiant Plains (plaine, foret, jungle) + base, et xxxx en niveau "spécifiques". Les grandes plaines verdoyantes et pleines de beauté de l'île. Surplombant les plages, ces zones

- Shield Coast (plage tropical aquatique) + militaire et "corruption" en niveau "spécifique". De grande plages ainsi que des îlots reposant sur un océan bleuté, qui ont cependant été en première loge lors de l'incident de Gosth Island d'il y a dix ans. Certaines parties sont donc interdites le temps que le nettoyage soit terminé. Quelques espaces sont cependant interdits, controllé par GUN.

- Crystal Mountain (montagne glace grotte) + technologie, base et volcan en niveau "spécifique". La grande montagne surplombant l'île. Contenant de nombreux tunnels, cette montagne offre de nombreux secret à qui s'y aventure. On dit qu'un projet de recherche d'énergie géothermique

- Lost Utopia (technologie base virtuel) + quelques tropes types natures ainsi que virtuel en niveau "spécifique". L'ancienne ville super technologique qui devait être le produit du projet Millenium. Cette base est désormais habité par pas mal de gens cherchant à ne pas être remarqué. Cela peut aller des membres du peuple Sidh voulant être éloigné du tourisme aux membres à notre cher ami le Dr. Robotnik.

- Gosth Island (technologie base militaire) + "out of this dimension" en niveau "spécifique". Une grande île fortifiée, abritant l'ancienne Gosth Central. Fortement gardée par GUN, l'accès à cet endroit est interdit. Vous ne serez pas trop à être nombreux si jamais vous devez y aller…


#### Proposition 2.1

L'idée est de tenter de créer une cohérence entre les niveaux en utilisant une thématique à la Shadow Shoot, les pierres précieuses. Cela participerait à créer une sorte d'aspect "commun" entre les différentes zones de l'île afin de donner l'impression d'un univers "uni". Cela se combine également au fait que Shadow Shoot sera l'une des inspirations du jeu, voir un mini-jeu dedans.

- Sunlit Metropolis -> Diamound Metropolis. Diamound Highway étant le premier niveau de Shadow Shoot, et étant une ville type Station Square de ce que peu en voir conviendrait parfaitement.

- Ancient Cliff -> Moonstone Valley/Cliff. Ce nom rajoute un côté "lunaire" à tout l'espace, ce qui rajoute de l'originalité. Cela fait référence à la fois à la présence de pierre (et fait donc penser aux ruines), à la lune (donc au mystique).

- Radiant Plains -> Jadeite Plains. Ce nom fait référence à la Jadeite Forest de Shadow Shoot

- Shield Coast -> Zircon Coast.

- Crystal Mountain -> Pearl Mountain. Ce nom est directement repris de Shadow Shoot.

- Lost Utopia -> Silicon Utopia. Référence direct à l'aspect hyper technologique de cette Utopie.

- Gosth Island -> Obsidian Island. L'obsidienne est sombre, et y'a souvent l'idée de l'obsidienne comme porte vers d'autres mondes.

#### Proposition 2.2

Cette proposition essaie de prendre comme thématique pour chaque lieu une référence à des concepts moraux et philosophiques, afin de créer une thématique peut-être plus "profonde". L'aventage d'une telle convention de nommage est qu'elle permettrait de créer une cohérence avec un nom de boss final à la "NonAgression" ou "Exception".

- Sunlit Metropolis -> Monado Town/Metropolis/City. *La référence aux monades de leibniz sert à mettre en avant le fait que cette ville est un petit monde à elle toute seule, et jouer sur le fait que la diversité de cette ville en fait d'une certaine manière un "univers".*

- Ancient Cliff -> Faith/Sacred/Holy Canyon/Valley. *Représente les croyances ancestrales du peuple Sidh, et le nombre de ruines ayant un caractère sacré/ou important pour eux. Représente également la vertue theologiale de la foi. D'un point de vue scénario, on pourrait en faire une étape important en étant une foi envers les autres, ce qui pourrait être intéressant pour développer soi Ankou, soit Taranis.*

- Radiant Plains -> Sublime/Aesthetics/Numinous Wilds/Grasses *Ici, le principe est de plus jouer sur l'aspect "esthétiques" des grandes pleines et forets verdoyantes de l'île.*

- Shield Coast -> Paradox/Aphoric Coast. *Une référence directe à sa corruption, ainsi qu'aux paradoxes qui la traverse. D'un côté c'est une belle côte tropicale, de l'autre un espace dangereux corrompu. C'est le lieu des Sidh les plus visible, qui se revandiquent comme porteur de leur culture, mais qui l'ont en partie rendu plus "mercantile". C'est un espace à la fois ouvert sur la plage et sur l'île corrompue. C'est l'océan et l'île. C'est le premier niveau et l'un des derniers. C'est le plus grand lieu accessible au touriste, mais pleins d'interduction soit pour leur sécurité, soit pour les secret de GUN.*

- Crystal Mountain -> Fortitude Mount. *Une montagne est toujours un endroit difficile à traverser, à*

- Lost Utopia -> Utopic Circuits. *Ici, l'idée est juste de créer une cohérence avec les autres, en mettant d'abord en avant l'aspect "utopie". Le but est aussi de bien mettre en avant la croyance en une utopie des gens qui on créé ce lieu, et le fait que leur idée n'étaient pas mauvaise, mais que ça s'est mal passé.*

- Gosth Island -> Wrath Island. *Référence directe aux éléments violents et tragique qui s'y sont passé.*

Boss final : Catharsis/Apathy
