## Levels in Sonic Boost game mode

## Correspondance de niveaux (mode classique)

- Niveau 1 : **Jadeite Forest** / **Calcite Hill**
- Niveau 2 : **Diamound Metropolis** / **Agate Park**
- Niveau 3 : **Peridot Archive** / **Zircon Desert**
- Niveau 4 : **Pearl Mountain** / **Carnelian Bridge**
- Niveau 5 : **Graphit Base** / **Moonstone Ruins**
- Niveau 6 : **Garnet Crater** / **Silicon Matrix**
- Niveau 7 : **Obsidian Station** / **Ametist Temples**
- Niveau Bonus : **Crystal Utopia**

## Coupes (mode tournoi)

- Chao Cup : Calcite Hill - ????? - ????? - ?????
- Ring Cup : ????? - ????? - ????? - ?????
- Emerald Cup : ????? - ????? - ????? - ?????
- Master Cup : ????? - ????? - ????? - **Crystal Cosmos**
