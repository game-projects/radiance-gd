# Sonic Radiance - Main Story - Act 4 : Le Peuple Sidh

## Synopsis

*TODO* : Construire comment on passe de la découverte du problème à ensuite l'act 6 qui représente un blackout total de l'ile (et se termine par le banissement des habitants de l'ile dans le monde stygien).

## Notes

Chaque act fait explorer en particulier une zone : L'act 1 représente pas mal la zone de plage, l'act 2 surtout une base d'Eggman et la zone qui sera autour (qui reste TBD), l'act 3 la base de GUN et l'act 4 représente pas mal. En prenant en compte le fait que l'act7 va représente surtout la Gosth Central, il reste à mettre en avant la zone de montagne (qui pourra être plus utile dans l'act 6), et celle de la ville.

Du coup, il serait intéressant de faire une grosse série de mission demandant d'explorer la ville. Par exemple, on pourrait se baser sur un besoin d'ancien artefact Sidh pris par les humains, ce qui permettrait de jouer sur vraiment cette "rivalité" entre les deux ?
